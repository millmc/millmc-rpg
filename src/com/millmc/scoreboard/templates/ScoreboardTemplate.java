package com.millmc.scoreboard.templates;

import org.bukkit.entity.Player;

public interface ScoreboardTemplate {

	ScoreboardTemplate TEMP1 = new Temp1Scoreboard();

	public void apply(Player player);

	public void updateMission(Player player, String text);

	public void updateCash(Player player, double cash);

	public void updateCoins(Player player, double coins);

	public void updateLevel(Player player, int level, double exp, double exp_max);

	public void updateKingdom(Player player, String tag);
}
