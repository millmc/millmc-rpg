package com.millmc.scoreboard.templates;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.millmc.Main;
import com.millmc.kingdom.Kingdom;
import com.millmc.kingdom.KingdomManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.scorebroad.line.LineBuilder;

public class Temp1Scoreboard implements ScoreboardTemplate {

	private DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###,###.##");
	private DecimalFormat df2 = new DecimalFormat("###,###,###,###,###,###,###,###");

	private LineBuilder lineBuilder;
	private UserManager userManager;

	private Set<String> setted;
	private KingdomManager kingdomManager;

	public Temp1Scoreboard() {
		this.lineBuilder = new LineBuilder();
		lineBuilder.addLine("", "§5", "");
		lineBuilder.addLine("", "§fReino: §a", "OneK");
		lineBuilder.addLine("", "§4", "");
		lineBuilder.addLine("", "§fMoedas: §a", "");
		lineBuilder.addLine("", "§fCash: §a", "");
		lineBuilder.addLine("", "§3", "");
		lineBuilder.addLine("", "§fLevel: §a", "");
		lineBuilder.addLine("", "§fExperiência: ", "");
		lineBuilder.addLine("", "§2", "");
		lineBuilder.addLine("", "§fMissão ativa:", "");
		lineBuilder.addLine("", "§f", "");
		lineBuilder.addLine("", "§1", "");
		lineBuilder.addLine("§6      millmc", "§6.com.br  ", "");

		this.setted = new HashSet<String>();

		this.userManager = Main.getInstance().getMillAPI().getModule().getUserManager();
		this.kingdomManager = Main.getInstance().getKingdomManager();
	}

	@Override
	public void apply(Player player) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = scoreboard.registerNewObjective("score", "score");

		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName("§6§lMILLMC");

		lineBuilder.buildLines(scoreboard, objective);

		player.setScoreboard(scoreboard);
		setted.add(player.getName());

		User user = userManager.getUser(player.getName());
		updateCash(player, user.getCash());

		updateCoins(player, user.getCoins());
		updateLevel(player, user.getLevel(), user.getExp(), user.getExpNeed());
		Kingdom kingdomById = kingdomManager.getKingdomById(user.getId());
		updateKingdom(player, kingdomById == null ? "§cNenhum" : kingdomById.getName());
	}

	@Override
	public void updateMission(Player player, String text) {
		if (!setted.contains(player.getName())) {
			return;
		}
		lineBuilder.updateLine(player, "§a" + text.substring(0, 14), 3);
	}

	@Override
	public void updateCash(Player player, double cash) {
		if (!setted.contains(player.getName())) {
			return;
		}
		String c = df2.format(cash);
		if (c.length() > 16) {
			c = c.substring(0, 16);
		}
		lineBuilder.updateLine(player, c, 9);
	}

	@Override
	public void updateCoins(Player player, double coins) {
		if (!setted.contains(player.getName())) {
			return;
		}
		String c = df.format(coins);
		if (c.length() > 16) {
			c = c.substring(0, 16);
		}
		lineBuilder.updateLine(player, c, 10);
	}

	@Override
	public void updateLevel(Player player, int level, double exp, double exp_max) {
		if (!setted.contains(player.getName())) {
			return;
		}
		String c = "§a" + df.format(exp) + "/" + df.format(exp_max);
		if (c.length() > 16) {
			c = c.substring(0, 16);
		}

		lineBuilder.updateLine(player, "" + level, 7);
		lineBuilder.updateLine(player, c, 6);
	}

	@Override
	public void updateKingdom(Player player, String tag) {
		lineBuilder.updateLine(player, tag, 12);
	}
}
