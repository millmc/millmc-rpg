package com.millmc.scoreboard;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.millmc.Main;
import com.millmc.classes.ClassManager;
import com.millmc.classes.Classe;
import com.millmc.kingdom.KingdomManager;
import com.millmc.scoreboard.templates.ScoreboardTemplate;
import com.millmcapi.general.groups.Group;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class ScoreboardManager {

	private ScoreboardTemplate template;
	private UserManager userManager;
	private GroupManager groupManager;
	private ClassManager classManager;

	public ScoreboardManager(ClassManager classManager, KingdomManager kingdomManager) {
		this.classManager = classManager;
		setTemplate(ScoreboardTemplate.TEMP1);

		Module module = Main.getInstance().getMillAPI().getModule();
		this.userManager = module.getUserManager();
		this.groupManager = module.getGroupManager();

		Bukkit.getPluginManager().registerEvents(new ScoreboardListener(this, kingdomManager), Main.getInstance());
	}

	public void setTemplate(ScoreboardTemplate template) {
		this.template = template;
		Bukkit.getOnlinePlayers().forEach(i -> template.apply(i));
		Main.getInstance().getLogger().log(Level.INFO, "scoreboard template changed to '" + template.getClass().getSimpleName() + "'");
	}

	public ScoreboardTemplate getTemplate() {
		return template;
	}

	public void handlerTab() {
		Bukkit.getOnlinePlayers().forEach(i -> {
			Scoreboard scoreboard = i.getScoreboard();
			if (scoreboard != null) {
				Bukkit.getOnlinePlayers().forEach(players -> {

					User user = userManager.getUser(players.getName());

					if (user != null) {

						Group group = groupManager.getGroup(user.getGroupId());
						Classe classe = classManager.getClasse(user.getClassName());
						String prefix = group.getPrefix();

						String teamName = group.getOrder() + players.getName();

						if (prefix.length() > 16) {
							prefix = prefix.substring(0, 16);
						}
						if (teamName.length() > 16) {
							teamName = teamName.substring(0, 16);
						}

						Team team = scoreboard.getTeam(teamName);
						if (team == null) {
							team = scoreboard.registerNewTeam(teamName);
						}
						team.setPrefix(prefix);

						if (classe != null) {
							team.setSuffix("§7 " + classe.getIcon());
						} else {
							team.setSuffix("");
						}

						if (!team.hasEntry(players.getName())) {
							team.addEntry(players.getName());
						}
					}
				});
			}
		});
	}
}
