package com.millmc.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.millmc.economy.custom.PlayerMoneyChangeEvent;
import com.millmc.kingdom.Kingdom;
import com.millmc.kingdom.KingdomManager;
import com.millmc.scoreboard.templates.Temp1Scoreboard;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.custom.PlayerCashChangeEvent;
import com.millmcapi.general.users.custom.PlayerExpChangeEvent;
import com.millmcapi.general.users.custom.PlayerLevelChangeEvent;
import com.millmcapi.general.users.custom.UserJoinEvent;
import com.millmcapi.general.users.custom.UserQuitEvent;
import com.millmcapi.spigot.event.update.async.AsyncUpdateEvent;
import com.millmcapi.spigot.event.update.async.AsyncUpdateType;

public class ScoreboardListener implements Listener {

	private ScoreboardManager scoreboardManager;
	private KingdomManager kingdomManager;

	public ScoreboardListener(ScoreboardManager scoreboardManager, KingdomManager kingdomManager) {
		this.scoreboardManager = scoreboardManager;
		this.kingdomManager = kingdomManager;
	}

	@EventHandler
	public void onMoney(PlayerMoneyChangeEvent event) {
		Player player = event.getPlayer();
		scoreboardManager.getTemplate().updateCoins(player, event.getLater());
	}

	@EventHandler
	public void onExp(PlayerExpChangeEvent event) {
		Player player = event.getPlayer();
		User user = event.getUser();
		scoreboardManager.getTemplate().updateLevel(player, user.getLevel(), user.getExp(), user.getExpNeed());
	}

	@EventHandler
	public void onLevel(PlayerLevelChangeEvent event) {
		Player player = event.getPlayer();
		User user = event.getUser();
		scoreboardManager.getTemplate().updateLevel(player, user.getLevel(), user.getExp(), user.getExpNeed());
	}

	@EventHandler
	public void onCash(PlayerCashChangeEvent event) {
		Player player = event.getPlayer();
		scoreboardManager.getTemplate().updateCash(player, event.getUser().getCash());
	}

	int count = 0;

	@EventHandler
	public void onTime(AsyncUpdateEvent event) {
		if (event.getUpdateType() == AsyncUpdateType.SEC_3) {
			scoreboardManager.handlerTab();

			for (Player player : Bukkit.getOnlinePlayers()) {
				Kingdom kingdomByPlayer = kingdomManager.getKingdomByPlayer(player);
				scoreboardManager.getTemplate().updateKingdom(player, kingdomByPlayer == null ? "§cNenhum" : kingdomByPlayer.getName());
			}
		} else if (event.getUpdateType() == AsyncUpdateType.FASTER) {

			String text = "Mate o porco e viva matando ";
			String out = "";
			for (int i = 0; i < text.length(); i++) {
				if (i >= count) {
					out += text.charAt(i);
				}
			}
			for (int i = 0; i < text.length(); i++) {
				if (i < count) {
					out += text.charAt(i);
				}
			}
			count += 1;
			if (count >= text.length()) {
				count = 0;
			}

			for (Player player : Bukkit.getOnlinePlayers()) {
				((Temp1Scoreboard) scoreboardManager.getTemplate()).updateMission(player, out);
			}
		}
	}

	@EventHandler
	public void onQuit(UserQuitEvent event) {
		scoreboardManager.handlerTab();
	}

	@EventHandler
	public void onJoin(UserJoinEvent event) {
		Player player = event.getPlayer();

		scoreboardManager.getTemplate().apply(player);
		scoreboardManager.handlerTab();
	}
}
