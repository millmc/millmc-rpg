package com.millmc.damage;

import org.bukkit.Bukkit;

import com.millmc.status.StatusManager;
import com.millmcapi.MillAPI;

public class CustomDamageManager {

	public CustomDamageManager(StatusManager statusManager) {

		Bukkit.getPluginManager().registerEvents(new CustomDamageListener(statusManager), MillAPI.getHost());
	}

}
