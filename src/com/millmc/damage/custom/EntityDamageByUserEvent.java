package com.millmc.damage.custom;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.millmcapi.general.users.User;

public class EntityDamageByUserEvent extends Event implements Cancellable {

	private boolean cancelled = false;

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	private static final HandlerList handlers = new HandlerList();
	private User damager;
	private Entity entity;
	private double damage;

	public EntityDamageByUserEvent(Entity entity, User damager, double damage) {
		this.entity = entity;
		this.damager = damager;
		this.damage = damage;
	}

	public Entity getEntity() {
		return this.entity;
	}

	public User getDamager() {
		return this.damager;
	}

	public double getDamage() {
		return this.damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
