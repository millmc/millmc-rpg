package com.millmc.damage.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.millmcapi.general.users.User;

public class UserDamageByUserEvent extends PlayerEvent implements Cancellable {

	private boolean cancelled = false;

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	private static final HandlerList handlers = new HandlerList();
	private User user;
	private User damager;
	private double damage;

	public UserDamageByUserEvent(Player player, User user, User damager, double damage) {
		super(player);
		this.user = user;
		this.damager = damager;
		this.damage = damage;
	}

	public User getUser() {
		return this.user;
	}

	public User getDamager() {
		return this.damager;
	}

	public double getDamage() {
		return this.damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
