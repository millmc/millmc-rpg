package com.millmc.damage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import com.millmc.damage.custom.EntityDamageByUserEvent;
import com.millmc.damage.custom.UserDamageByAnimalEvent;
import com.millmc.damage.custom.UserDamageByUserEvent;
import com.millmc.damage.custom.UserDamageEvent;
import com.millmc.status.Status;
import com.millmc.status.StatusManager;
import com.millmc.status.constructors.Stats;
import com.millmcapi.MillAPI;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CustomDamageListener implements Listener {

	private UserManager userManager;
	private StatusManager statusManager;
	private Map<String, Integer> countToEnergy;

	public CustomDamageListener(StatusManager statusManager) {
		this.statusManager = statusManager;
		this.userManager = MillAPI.getInstance().getModule().getUserManager();
		this.countToEnergy = new HashMap<String, Integer>();
	}

	public boolean isEnergy(Player player) {
		if (countToEnergy.containsKey(player.getName())) {
			int count = countToEnergy.get(player.getName()) + 1;
			if (count == 5) {
				countToEnergy.put(player.getName(), 0);
				return true;
			}
		} else {
			countToEnergy.put(player.getName(), 1);
		}
		return false;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onDamage(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			User user = userManager.getUser(player);

			if (event.getDamager() instanceof Player) {
				Player damager = (Player) event.getDamager();
				User userDamager = userManager.getUser(damager);

				event.setDamage(0.0);
				double damage = damage(user, userDamager);
				if (damage <= 0.0) {
					event.setCancelled(true);
					return;
				}
				UserDamageByUserEvent userDamageByUserEvent = new UserDamageByUserEvent(player, user, userDamager, damage);

				Bukkit.getPluginManager().callEvent(userDamageByUserEvent);

				if (userDamageByUserEvent.isCancelled()) {
					event.setCancelled(true);
					return;
				}

				if (player.isDead()) {
					event.setCancelled(true);
					return;
				}

				player.damage(userDamageByUserEvent.getDamage());

			} else if (event.getDamager() instanceof Animals || event.getDamager() instanceof Monster) {
				event.setDamage(0.0);
				double damage = damage(user, event.getDamager());
				if (damage <= 0.0) {
					event.setCancelled(true);
					return;
				}
				UserDamageByAnimalEvent userDamageByAnimalEvent = new UserDamageByAnimalEvent(player, user, event.getEntity(), damage);

				Bukkit.getPluginManager().callEvent(userDamageByAnimalEvent);

				if (userDamageByAnimalEvent.isCancelled()) {
					event.setCancelled(true);
					return;
				}

				if (player.isDead()) {
					event.setCancelled(true);
					return;
				}

				user.getPlayer().damage(userDamageByAnimalEvent.getDamage());
			} else {
				if (event.getDamager() instanceof Projectile) {
					Projectile pj = (Projectile) event.getDamager();
					if (pj.getShooter() instanceof Animals || pj.getShooter() instanceof Monster) {
						event.setDamage(0.0);
						double damage = damage(user, event.getDamager());
						if (damage <= 0.0) {
							event.setCancelled(true);
							return;
						}

						UserDamageByAnimalEvent userDamageByAnimalEvent = new UserDamageByAnimalEvent(player, user, event.getEntity(), damage);

						Bukkit.getPluginManager().callEvent(userDamageByAnimalEvent);

						if (userDamageByAnimalEvent.isCancelled()) {
							event.setCancelled(true);
							return;
						}

						if (player.isDead()) {
							event.setCancelled(true);
							return;
						}

						player.damage(userDamageByAnimalEvent.getDamage());
						return;
					}
				}

				Player damager = extract(event.getDamager());
				if (damager == null) {
					return;
				}
				event.setDamage(0.0);
				User userDamager = userManager.getUser(damager);
				double damage = damage(user, userDamager);
				if (damage <= 0.0) {
					event.setCancelled(true);
					return;
				}
				UserDamageByUserEvent userDamageByUserEvent = new UserDamageByUserEvent(player, user, userDamager, damage);

				Bukkit.getPluginManager().callEvent(userDamageByUserEvent);

				if (userDamageByUserEvent.isCancelled()) {
					event.setCancelled(true);
					return;
				}

				if (player.isDead()) {
					event.setCancelled(true);
					return;
				}

				player.damage(userDamageByUserEvent.getDamage());
			}
		} else if (event.getEntity() instanceof Animals || event.getEntity() instanceof Monster) {

			Player damager = extract(event.getDamager());
			if (damager == null) {
				return;
			}

			event.setDamage(0.0);
			User userDamager = userManager.getUser(damager);
			double damage = damage(event.getEntity(), userDamager);
			if (damage <= 0.0) {
				event.setCancelled(true);
				return;
			}

			EntityDamageByUserEvent entityDamageByUserEvent = new EntityDamageByUserEvent(event.getEntity(), userDamager, damage);

			Bukkit.getPluginManager().callEvent(entityDamageByUserEvent);

			if (entityDamageByUserEvent.isCancelled()) {
				event.setCancelled(true);
				return;
			}

			if (event.getEntity().isDead()) {
				event.setCancelled(true);
				return;
			}

			if (event.getEntity() instanceof Animals) {
				((Animals) event.getEntity()).damage(entityDamageByUserEvent.getDamage());
			} else if (event.getEntity() instanceof Monster) {
				((Monster) event.getEntity()).damage(entityDamageByUserEvent.getDamage());
			}
		}
	}

	public double damage(Entity entity, User damager) {
		Status statusDamager = statusManager.getStatus(damager.getName());

		// all for damage
		double damage = statusDamager.getDamage(statusDamager.getLevelStats(Stats.DAMAGE));
		double damageCrit = statusDamager.getDamageCrit(statusDamager.getLevelStats(Stats.CRITICAL));
		int chanceCrit = statusDamager.getChanceCrit(statusDamager.getLevelStats(Stats.CRITICAL_CHANCE));

		if (chanceCrit > 0) {
			int random = ThreadLocalRandom.current().nextInt(0, 100);
			if (random <= chanceCrit) {
				damage += damageCrit;
			}
		}
		if (damager.getClassName().equalsIgnoreCase("Humano")) {
			if (isEnergy(damager.getPlayer())) {
				damage += 4.0;
			}
		}
		// end of damage

		if (entity instanceof Animals) {
			((Animals) entity).damage(damage);
		} else if (entity instanceof Monster) {
			((Monster) entity).damage(damage);
		}
		return damage;
	}

	public double damage(User player, User damager) {
		Status statusPlayer = statusManager.getStatus(player.getName());
		Status statusDamager = statusManager.getStatus(damager.getName());

		// all for damage
		double damage = statusDamager.getDamage(statusDamager.getLevelStats(Stats.DAMAGE));
		double damageCrit = statusDamager.getDamageCrit(statusDamager.getLevelStats(Stats.CRITICAL));
		int chanceCrit = statusDamager.getChanceCrit(statusDamager.getLevelStats(Stats.CRITICAL_CHANCE));

		if (chanceCrit > 0) {
			int random = ThreadLocalRandom.current().nextInt(0, 100);
			if (random <= chanceCrit) {
				damage += damageCrit;
			}
		}
		if (damager.getClassName().equalsIgnoreCase("Humano")) {
			if (isEnergy(damager.getPlayer())) {
				damage += 4.0;
			}
		}
		// end of damage

		// start try block damage
		int chanceBlock = statusPlayer.getChanceBlock(statusPlayer.getLevelStats(Stats.BLOCK));
		if (chanceBlock > 0) {
			int random = ThreadLocalRandom.current().nextInt(0, 100);
			if (random <= chanceBlock) {
				Player playerPlayer = player.getPlayer();
				playerPlayer.getWorld().playSound(playerPlayer.getLocation(), Sound.ANVIL_LAND, 1.3f, 1.3f);
				return 0.0;
			}
		}

		// stop try block damage
		// player.getPlayer().damage(damage);
		return damage;
	}

	public double damage(User player, Entity damager) {
		Status statusPlayer = statusManager.getStatus(player.getName());

		// start try block damage
		int chanceBlock = statusPlayer.getChanceBlock(statusPlayer.getLevelStats(Stats.BLOCK));
		if (chanceBlock > 0) {
			int random = ThreadLocalRandom.current().nextInt(0, 100);
			if (random <= chanceBlock) {
				Player playerPlayer = player.getPlayer();
				playerPlayer.getWorld().playSound(playerPlayer.getLocation(), Sound.ANVIL_LAND, 1.3f, 1.3f);
				return 0.0;
			}
		}

		// stop try block damage
		// player.getPlayer().damage(0.0);
		return 0.0;
	}

	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			User user = userManager.getUser(player);

			UserDamageEvent userDamageEvent = new UserDamageEvent(player, user);
			if (userDamageEvent.isCancelled()) {
				event.setCancelled(true);
				return;
			}
			Bukkit.getPluginManager().callEvent(userDamageEvent);
		}
	}

	public Player extract(Entity entity) {
		Player player = null;
		if (entity instanceof Player) {
			player = (Player) entity;
		} else if (entity instanceof Projectile) {
			Projectile pj = (Projectile) entity;
			if (pj.getShooter() instanceof Player) {
				player = (Player) pj.getShooter();
			}
		}
		return player;
	}

}
