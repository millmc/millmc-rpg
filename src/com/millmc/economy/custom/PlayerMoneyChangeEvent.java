package com.millmc.economy.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerMoneyChangeEvent extends Event {

	public final HandlerList handlerList = new HandlerList();
	private Player player;
	private double later;

	public PlayerMoneyChangeEvent(Player player, double later) {
		this.player = player;
		this.later = later;
	}

	public Player getPlayer() {
		return player;
	}

	public double getLater() {
		return later;
	}

	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
