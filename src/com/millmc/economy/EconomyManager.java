package com.millmc.economy;

import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.economy.command.CommandMoney;
import com.millmc.economy.top.TopManager;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class EconomyManager {

	private TopManager topManager;
	private UserManager userManager;

	public EconomyManager() {
		Module module = Main.getInstance().getMillAPI().getModule();
		this.userManager = module.getUserManager();
		GroupManager groupManager = module.getGroupManager();

		this.topManager = new TopManager(groupManager);

		Main.getInstance().getMillAPI().getCommand().registerCommand(new CommandMoney(this, userManager, groupManager));
	}

	public double getBalance(String name) {
		User user = userManager.getUser(name);
		return user.getCoins();
	}

	// withdraw

	public void withdrawPlayer(String name, double valor) {
		User user = userManager.getUser(name);
		user.removeCoins(valor);
		userManager.update(user);
	}

	// deposit

	public void depositPlayer(String name, double valor) {
		User user = userManager.getUser(name);
		user.addCoins(valor);
		userManager.update(user);
	}

	public TopManager getTopManager() {
		return this.topManager;
	}

	public boolean hasBalance(Player name, int coins) {
		User user = userManager.getUser(name.getName());
		return user.getCoins() >= coins;
	}
}
