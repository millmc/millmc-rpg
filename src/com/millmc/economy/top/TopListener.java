package com.millmc.economy.top;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.millmcapi.spigot.event.update.async.AsyncUpdateEvent;
import com.millmcapi.spigot.event.update.async.AsyncUpdateType;

public class TopListener implements Listener {

	private TopManager topManager;

	public TopListener(TopManager topManager) {
		this.topManager = topManager;
	}

	@EventHandler
	public void onUpdate(AsyncUpdateEvent event) {
		if (event.getUpdateType() == AsyncUpdateType.MINUTE_5) {
			System.out.println("[Top] Starting update top coins...");
			topManager.updateTopCoins();
			System.out.println("[Top] Top coins updated!");
		}
	}

}
