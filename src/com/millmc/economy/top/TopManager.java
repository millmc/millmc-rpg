package com.millmc.economy.top;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class TopManager {

	private List<String> tops;
	private GroupManager groupManager;
	private UserManager userManager;

	public TopManager(GroupManager groupManager) {
		this.userManager = Main.getInstance().getMillAPI().getModule().getUserManager();
		this.groupManager = groupManager;
		this.tops = new ArrayList<String>();
	}

	public void sendMessageTop(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			player.sendMessage("");
			player.sendMessage("§7Exibindo os §a10 primeiros §7colocados em moedas!");
			player.sendMessage("");

			for (int x = 0; x < 10; x++) {
				String name = tops.get(x).split(" com ")[0];
				double money = Double.valueOf(tops.get(x).split(" com ")[1]);

				String tag = getTag(name).replaceAll(" ", "");
				player.sendMessage("§a " + (x + 1) + "º §7Lugar, " + tag + " " + name + "§7 com §a" + format(money));
			}
			player.sendMessage("");
		});
	}

	public String format(double money) {
		DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###.##");
		return df.format(money);
	}

	public void updateTopCoins() {
		tops.clear();

		try {
			Connection connection = Main.getInstance().getMillAPI().getLocalConnection().getConnection();
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM `user` WHERE `coins` > 0 ORDER BY `coins` DESC LIMIT 10");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				tops.add(userManager.getUserTemporality(rs.getInt("id")).getName() + " com " + rs.getDouble("coins"));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<String> getTops() {
		return tops;
	}

	public String getTag(String name) {
		User user = userManager.getUser(name);
		if (user == null) {
			user = userManager.getUserTemporality(name);
		}
		if (user == null) {
			return "";
		}
		return groupManager.getGroup(user.getGroupId()).getPrefix();
	}

}
