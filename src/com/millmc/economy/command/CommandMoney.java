package com.millmc.economy.command;

import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.millmc.economy.EconomyManager;
import com.millmc.economy.custom.PlayerMoneyChangeEvent;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandMoney extends Command {

	private EconomyManager economyManager;
	private UserManager userManager;
	private GroupManager groupManager;

	public CommandMoney(EconomyManager economyManager, UserManager userManager, GroupManager groupManager) {
		super("moedas");
		this.economyManager = economyManager;
		this.userManager = userManager;
		this.groupManager = groupManager;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player.getName());

		if (args.length == 0) {
			player.sendMessage("§7Você possui: §a" + format(economyManager.getBalance(player.getName())) + " moedas.");
			return false;
		} else if (args.length >= 1) {

			if (args[0].equalsIgnoreCase("pay") || args[0].equalsIgnoreCase("pagar")) {
				if (args.length == 1) {
					player.sendMessage("§c/" + this.getName() + " §apagar <jogador> <quantia>");
					return false;
				}

				String name = Bukkit.getOfflinePlayer(args[1]).getName();

				int amountCoins = 0;

				try {
					amountCoins = Integer.parseInt(args[2]);
				} catch (NumberFormatException e) {
					player.sendMessage("§7Utilize apenas números na quantia!");
					return false;
				}

				if (amountCoins <= 0) {
					player.sendMessage("§7É necessário que a quantia seja maior que 0!");
					return false;
				}

				if (!economyManager.hasBalance(player, amountCoins)) {
					player.sendMessage("§7Você não possui essa quantia para enviar!");
					return false;
				}

				economyManager.withdrawPlayer(player.getName(), amountCoins);
				Bukkit.getPluginManager().callEvent(new PlayerMoneyChangeEvent(player, economyManager.getBalance(player.getName())));

				economyManager.depositPlayer(name, amountCoins);
				player.sendMessage("§7Você transferiu §a" + format(amountCoins) + " moedas §7para §a" + name);

				Player player2 = Bukkit.getPlayer(name);

				if (player2 != null) {
					player2.sendMessage("§7Você recebeu §a" + format(amountCoins) + " moedas §7de §a" + player.getName());

					Bukkit.getPluginManager().callEvent(new PlayerMoneyChangeEvent(player2, economyManager.getBalance(player2.getName())));

				}
				return false;
			}

			if (groupManager.check(user.getGroupId(), 80)) {
				if (args[0].equalsIgnoreCase("give")) {
					if (args.length == 1) {
						player.sendMessage("§c/" + this.getName() + " §agive <jogador> <quantia>");
						return false;
					}

					String name = Bukkit.getOfflinePlayer(args[1]).getName();

					int quantia = 0;

					try {
						quantia = Integer.parseInt(args[2]);
					} catch (NumberFormatException e) {
						player.sendMessage("§7Utilize apenas números na quantia!");
						return false;
					}

					if (quantia <= 0) {
						player.sendMessage("§7É necessário que a quantia seja maior que 0!");
						return false;
					}

					economyManager.depositPlayer(name, quantia);

					player.sendMessage("§7Você deu §a" + format(quantia) + " moedas §7para §a" + name);

					Player player2 = Bukkit.getPlayer(name);

					if (player2 != null) {
						player2.sendMessage("§7Você recebeu §a" + format(quantia) + " moedas §7de §a" + player.getName());

						Bukkit.getPluginManager().callEvent(new PlayerMoneyChangeEvent(player2, economyManager.getBalance(player2.getName())));

					}
					return false;
				}

				if (args[0].equalsIgnoreCase("take")) {
					if (args.length == 1) {
						player.sendMessage("§c/" + this.getName() + " §atake <jogador> <quantia>");
						return false;
					}

					String name = Bukkit.getOfflinePlayer(args[1]).getName();

					int quantia = 0;

					try {
						quantia = Integer.parseInt(args[2]);
					} catch (NumberFormatException e) {
						player.sendMessage("§7Utilize apenas números na quantia!");
						return false;
					}

					if (quantia <= 0) {
						player.sendMessage("§7É necessário que a quantia seja maior que 0!");
						return false;
					}

					economyManager.withdrawPlayer(name, quantia);

					player.sendMessage("§7Você removeu §a" + format(quantia) + " moedas §7de §a" + name);

					Player player2 = Bukkit.getPlayer(name);

					if (player2 != null) {
						player2.sendMessage("§7Você perdeu §a" + format(quantia) + " moedas §7, retirado por §a" + player.getName());
						Bukkit.getPluginManager().callEvent(new PlayerMoneyChangeEvent(player2, economyManager.getBalance(player2.getName())));

					}
					return false;
				}
			}

			if (args[0].equalsIgnoreCase("top")) {
				economyManager.getTopManager().sendMessageTop(player);
				return false;
			}

			player.sendMessage("§aComandos disponíveis.");
			player.sendMessage("§7/" + arg1 + " §apagar <jogador> <quantia>");
			player.sendMessage("§7/" + arg1 + " §a<jogador>");
			player.sendMessage("§7/" + arg1 + " §atop");
			if (groupManager.check(user.getGroupId(), 80)) {
				player.sendMessage("§7/" + arg1 + " §agive <jogador> <quantia>");
				player.sendMessage("§7/" + arg1 + " §atake <jogador> <quantia>");
			}
		}
		return false;
	}

	public String format(double money) {
		DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###.##");
		return df.format(money);
	}

}
