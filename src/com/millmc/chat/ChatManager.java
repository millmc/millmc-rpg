package com.millmc.chat;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.chat.commands.CommandChat;
import com.millmc.chat.commands.CommandGlobal;
import com.millmc.chat.commands.tell.CommandTell;
import com.millmc.kingdom.KingdomManager;
import com.millmc.title.TitleManager;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.command.Command;

public class ChatManager {

	private Map<ChatType, Boolean> chatsActive;
	private Map<String, Map<ChatType, Boolean>> playerChat;

	public ChatManager(TitleManager titleManager, KingdomManager kingdomManager) {
		Module module = Main.getInstance().getMillAPI().getModule();
		UserManager userManager = module.getUserManager();
		GroupManager groupManager = module.getGroupManager();

		this.chatsActive = new HashMap<ChatType, Boolean>();
		this.playerChat = new HashMap<String, Map<ChatType, Boolean>>();

		for (ChatType chatType : ChatType.values()) {
			setActive(chatType, true);
		}

		Command command = Main.getInstance().getMillAPI().getCommand();
		command.registerCommand(new CommandChat(this, userManager, groupManager));
		command.registerCommand(new CommandGlobal(this, userManager, groupManager, titleManager, kingdomManager));
		command.registerCommand(new CommandTell(this, userManager, groupManager));

		Bukkit.getPluginManager().registerEvents(new ChatListener(this, userManager, groupManager, titleManager, kingdomManager), Main.getInstance());
	}

	public boolean isActive(ChatType chatType) {
		return chatsActive.get(chatType);
	}

	public void setActive(ChatType chatType, boolean status) {
		chatsActive.put(chatType, status);
	}

	public boolean isActive(Player player, ChatType chatType) {
		if (playerChat.containsKey(player.getName())) {
			if (playerChat.get(player.getName()).containsKey(chatType)) {
				return playerChat.get(player.getName()).get(chatType);
			}
		}
		return false;
	}

	public void setActive(Player player, ChatType chatType, boolean status) {
		if (playerChat.containsKey(player.getName())) {
			Map<ChatType, Boolean> map = playerChat.get(player.getName());
			map.put(chatType, status);
			playerChat.put(player.getName(), map);
		} else {
			Map<ChatType, Boolean> map = new HashMap<ChatType, Boolean>();
			map.put(chatType, status);
			playerChat.put(player.getName(), map);
		}
	}

}
