package com.millmc.chat;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.millmc.kingdom.Kingdom;
import com.millmc.kingdom.KingdomManager;
import com.millmc.title.TitleManager;
import com.millmc.title.TitlePlayer;
import com.millmc.title.interfaces.Title;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.general.users.custom.UserJoinEvent;

public class ChatListener implements Listener {

	private ChatManager chatManager;
	private UserManager userManager;
	private GroupManager groupManager;

	private Map<String, Long> cooldown;
	private TitleManager titleManager;
	private KingdomManager kingdomManager;

	public ChatListener(ChatManager chatManager, UserManager userManager, GroupManager groupManager, TitleManager titleManager, KingdomManager kingdomManager) {
		this.chatManager = chatManager;
		this.userManager = userManager;
		this.groupManager = groupManager;
		this.titleManager = titleManager;
		this.kingdomManager = kingdomManager;

		this.cooldown = new HashMap<String, Long>();
	}

	@EventHandler
	public void onJoin(UserJoinEvent event) {
		Player player = event.getPlayer();

		chatManager.setActive(player, ChatType.LOCAL_PLAYER, true);
		chatManager.setActive(player, ChatType.GLOBAL_PLAYER, true);
		chatManager.setActive(player, ChatType.TELL_PLAYER, true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onChat(AsyncPlayerChatEvent event) {
		if (event.isCancelled()) {
			return;
		}

		Player player = event.getPlayer();
		String message = event.getMessage();
		event.setCancelled(true);

		if (cooldown.containsKey(player.getName()) && cooldown.get(player.getName()) > System.currentTimeMillis()) {
			player.sendMessage("§cAguarde para enviar outra mensagem no chat local!");
			return;
		}

		User user = userManager.getUser(player.getName());

		if (user.getClassName().equalsIgnoreCase("nothing")) {
			player.sendMessage("§7É necessário escolher sua classe para poder falar no chat!");
			return;
		}

		if (!chatManager.isActive(ChatType.LOCAL) && !groupManager.check(user.getGroupId(), 60)) {
			player.sendMessage("§cO chat local está desativado!");
			return;
		}

		if (!chatManager.isActive(player, ChatType.LOCAL_PLAYER)) {
			player.sendMessage("§cVocê está com o chat local desativado!");
			return;
		}

		TitlePlayer titlePlayer = titleManager.getTitlePlayer(player);
		Title title = titleManager.getTitle(titlePlayer.getTitleUsing());
		String titleName = title.getName(titlePlayer.getLevel(title));

		Kingdom kingdomById = kingdomManager.getKingdomById(user.getId());
		String kingdomTag = kingdomById == null ? "" : "§7[" + kingdomById.getName() + "§7] ";

		String msg = "§e[L] " + kingdomTag + "§8[" + titleName + "] " + groupManager.getGroup(user.getGroupId()).getPrefix() + "§7[" + user.getLevel() + "] "
				+ player.getName() + ": §e" + message;

		player.sendMessage(msg);

		cooldown.put(player.getName(), System.currentTimeMillis() + 1000);

		int count = 0;
		for (Entity entity : player.getNearbyEntities(60, 60, 60)) {
			if (entity instanceof Player) {
				Player target = (Player) entity;
				if (!target.equals(player)) {
					if (chatManager.isActive(target, ChatType.LOCAL_PLAYER)) {
						count += 1;
						target.sendMessage(msg);
					}
				}
			}
		}

		if (count <= 0) {
			player.sendMessage("§eNão há jogadores próximos!");
		}
	}

}
