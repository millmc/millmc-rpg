package com.millmc.chat.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.millmc.chat.ChatManager;
import com.millmc.chat.ChatType;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandChat extends Command {

	private ChatManager chatManager;
	private UserManager userManager;
	private GroupManager groupManager;

	public CommandChat(ChatManager chatManager, UserManager userManager, GroupManager groupManager) {
		super("chat");
		this.chatManager = chatManager;
		this.userManager = userManager;
		this.groupManager = groupManager;
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {

		if (!(sender instanceof Player)) {
			return false;
		}

		// LOCAL,
		// GLOBAL,
		// TELL,
		//
		// LOCAL_PLAYER,
		// GLOBAL_PLAYER,
		// TELL_PLAYER;

		Player player = (Player) sender;
		User user = userManager.getUser(player.getName());

		if (args.length == 0) {
			player.sendMessage("§7Chats que podem ser desativados: §aLocal, Global, Tell");
		} else if (args.length >= 1) {
			if (args.length == 2 && args[1].equalsIgnoreCase("geral") && groupManager.check(user.getGroupId(), 60)) {
				ChatType chatType = null;
				try {
					chatType = ChatType.valueOf(args[0].toUpperCase());
				} catch (Exception e) {
				}

				if (chatType == null) {
					player.sendMessage("§cNão foi encontrado este tipo de chat!");
					return false;
				}

				if (chatType != ChatType.GLOBAL && chatType != ChatType.LOCAL && chatType != ChatType.TELL) {
					player.sendMessage("§cNão foi encontrado este tipo de chat!");
					return false;
				}

				boolean active = !chatManager.isActive(chatType);
				chatManager.setActive(chatType, active);
				player.sendMessage("§7Você alterou o status do chat geral §a" + args[0] + " §7para §a" + (active ? "ativado" : "desativado"));
				return false;
			}

			ChatType chatType = null;
			try {
				chatType = ChatType.valueOf(args[0].toUpperCase() + "_PLAYER");
			} catch (Exception e) {
			}

			if (chatType == null) {
				player.sendMessage("§cNão foi encontrado este tipo de chat!");
				return false;
			}

			if (chatType != ChatType.GLOBAL_PLAYER && chatType != ChatType.LOCAL_PLAYER && chatType != ChatType.TELL_PLAYER) {
				player.sendMessage("§cNão foi encontrado este tipo de chat!");
				return false;
			}

			boolean active = !chatManager.isActive(player, chatType);
			chatManager.setActive(player, chatType, active);
			player.sendMessage("§7Você alterou o status do seu chat §a" + args[0] + " §7para §a" + (active ? "ativado" : "desativado"));
			return false;
		}

		return false;
	}

}
