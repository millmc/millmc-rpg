package com.millmc.chat.commands.tell;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.millmc.chat.ChatManager;
import com.millmc.chat.ChatType;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandTell extends Command {

	private ChatManager chatManager;
	private UserManager userManager;
	private GroupManager groupManager;

	private Map<String, Long> cooldown;

	public CommandTell(ChatManager chatManager, UserManager userManager, GroupManager groupManager) {
		super("tell");
		this.chatManager = chatManager;
		this.userManager = userManager;
		this.groupManager = groupManager;

		this.cooldown = new HashMap<String, Long>();
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {

		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player.getName());

		if (args.length == 0) {
			player.sendMessage("§7Use o comando: §a/tell <jogador> <mensagem>");
			return false;
		} else if (args.length >= 1) {

			if (cooldown.containsKey(player.getName()) && cooldown.get(player.getName()) > System.currentTimeMillis()) {
				player.sendMessage("§cAguarde para enviar outra mensagem no tell!");
				return false;
			}

			if (!chatManager.isActive(ChatType.TELL) && !groupManager.check(user.getGroupId(), 60)) {
				player.sendMessage("§cO tell global está desativado!");
				return false;
			}

			if (!chatManager.isActive(player, ChatType.TELL_PLAYER)) {
				player.sendMessage("§cVocê está com o tell desativado!");
				return false;
			}

			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				player.sendMessage("§cEste jogador não está conectado!");
				return false;
			}

			if (target.equals(player)) {
				player.sendMessage("§cEste jogador é você!");
				return false;
			}

			if (!chatManager.isActive(target, ChatType.TELL_PLAYER)) {
				player.sendMessage("§cEste jogador está com o tell desativado!");
				return false;
			}

			if (args.length == 1) {
				player.sendMessage("§cSua mensagem precisa ser maior que 3 caracteres!");
				return false;
			}

			StringBuilder sb = new StringBuilder();

			for (int x = 1; x < args.length; x++) {
				sb.append(args[x]);
				if (x != args.length) {
					sb.append(" ");
				}
			}

			if (sb.toString().length() <= 3) {
				player.sendMessage("§cSua mensagem precisa ser maior que 3 caracteres!");
				return false;
			}

			player.sendMessage("§7Para §2" + target.getName() + ": §7" + sb.toString());
			target.sendMessage("§7De §2" + player.getName() + ": §7" + sb.toString());

			cooldown.put(player.getName(), System.currentTimeMillis() + 500);
			return false;
		}

		return false;
	}

}
