package com.millmc.chat.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.millmc.Main;
import com.millmc.chat.ChatManager;
import com.millmc.chat.ChatType;
import com.millmc.kingdom.Kingdom;
import com.millmc.kingdom.KingdomManager;
import com.millmc.title.TitleManager;
import com.millmc.title.TitlePlayer;
import com.millmc.title.interfaces.Title;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandGlobal extends Command {

	private ChatManager chatManager;
	private UserManager userManager;
	private GroupManager groupManager;

	private Map<String, Long> cooldown;
	private TitleManager titleManager;
	private KingdomManager kingdomManager;

	public CommandGlobal(ChatManager chatManager, UserManager userManager, GroupManager groupManager, TitleManager titleManager, KingdomManager kingdomManager) {
		super("global", "", "", Arrays.asList("g"));
		this.chatManager = chatManager;
		this.userManager = userManager;
		this.groupManager = groupManager;
		this.titleManager = titleManager;
		this.kingdomManager = kingdomManager;

		this.cooldown = new HashMap<String, Long>();
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player.getName());

		if (args.length == 0) {
			player.sendMessage("§7Use o comando §a/" + arg1 + " <mensagem>");
		} else if (args.length >= 1) {
			if (cooldown.containsKey(player.getName()) && cooldown.get(player.getName()) > System.currentTimeMillis()) {
				player.sendMessage("§cAguarde para enviar outra mensagem no chat global!");
				return false;
			}

			if (!chatManager.isActive(ChatType.GLOBAL) && !groupManager.check(user.getGroupId(), 60)) {
				player.sendMessage("§cO chat global está desativado!");
				return false;
			}

			if (!chatManager.isActive(player, ChatType.GLOBAL_PLAYER)) {
				player.sendMessage("§cVocê está com o chat global desativado!");
				return false;
			}

			StringBuilder sb = new StringBuilder();

			for (int x = 0; x < args.length; x++) {
				sb.append(args[x]);
				if (x != args.length) {
					sb.append(" ");
				}
			}

			if (sb.toString().length() <= 3) {
				player.sendMessage("§cSua mensagem precisa ser maior que 3 caracteres!");
				return false;
			}
			TitlePlayer titlePlayer = titleManager.getTitlePlayer(player);
			Title title = titleManager.getTitle(titlePlayer.getTitleUsing());
			String titleName = title.getName(titlePlayer.getLevel(title));

			Kingdom kingdomById = kingdomManager.getKingdomById(user.getId());
			String kingdomTag = kingdomById == null ? "" : "§7[" + kingdomById.getName() + "§7] ";

			String msg = "§7[G] " + kingdomTag + "§8[" + titleName + "] " + groupManager.getGroup(user.getGroupId()).getPrefix() + "§7[" + user.getLevel() + "] "
					+ player.getName() + ": §7" + sb.toString();

			cooldown.put(player.getName(), System.currentTimeMillis() + 10000);

			new BukkitRunnable() {

				@Override
				public void run() {
					for (Player target : Bukkit.getOnlinePlayers()) {
						if (chatManager.isActive(target, ChatType.GLOBAL_PLAYER)) {
							target.sendMessage(msg);
						}
					}
				}
			}.runTaskAsynchronously(Main.getInstance());
		}
		return false;
	}

}
