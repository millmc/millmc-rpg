package com.millmc.chat;

public enum ChatType {

	LOCAL,
	GLOBAL,
	TELL,

	LOCAL_PLAYER,
	GLOBAL_PLAYER,
	TELL_PLAYER;

}
