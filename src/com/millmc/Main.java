package com.millmc;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import com.millmc.chat.ChatManager;
import com.millmc.classes.ClassManager;
import com.millmc.damage.CustomDamageManager;
import com.millmc.economy.EconomyManager;
import com.millmc.kingdom.KingdomManager;
import com.millmc.listener.GlobalListeners;
import com.millmc.mobs.MobManager;
import com.millmc.scoreboard.ScoreboardManager;
import com.millmc.shop.ShopManager;
import com.millmc.status.StatusManager;
import com.millmc.title.TitleManager;
import com.millmcapi.MillAPI;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.module.ModuleType;

public class Main extends JavaPlugin {

	private static Main instance;

	public static Main getInstance() {
		return instance;
	}

	private MillAPI millAPI;
	private ScoreboardManager scoreboardManager;
	private ChatManager chatManager;
	private EconomyManager economyManager;
	private ShopManager shopManager;
	private TitleManager titleManager;
	private StatusManager statusManager;
	private CustomDamageManager customDamageManager;
	private ClassManager classManager;
	private MobManager mobManager;
	private KingdomManager kingdomManager;

	@Override
	public void onEnable() {
		instance = this;

		this.millAPI = new MillAPI();
		millAPI.initializeApi(instance);

		millAPI.setGlobalConnection("158.69.55.199", "mine_3192", "mine_3192", "1e3dfca02c");
		millAPI.setLocalConnection("158.69.55.199", "mine_3192", "mine_3192", "1e3dfca02c");

		Module module = millAPI.getModule();
		module.InitializeModule(ModuleType.USERMANAGER);
		module.InitializeModule(ModuleType.GROUP);
		module.InitializeModule(ModuleType.SCOREBOARD);
		module.InitializeModule(ModuleType.NPC);
		module.InitializeModule(ModuleType.HOLOGRAM);

		this.kingdomManager = new KingdomManager();
		this.statusManager = new StatusManager();
		this.customDamageManager = new CustomDamageManager(statusManager);
		this.classManager = new ClassManager();

		this.economyManager = new EconomyManager();
		this.scoreboardManager = new ScoreboardManager(classManager, kingdomManager);
		this.shopManager = new ShopManager();
		this.titleManager = new TitleManager();

		this.chatManager = new ChatManager(titleManager, kingdomManager);
		this.mobManager = new MobManager();

		Bukkit.getPluginManager().registerEvents(new GlobalListeners(), this);
	}

	@Override
	public void onDisable() {

		HandlerList.unregisterAll(instance);
	}

	public MillAPI getMillAPI() {
		return this.millAPI;
	}

	public KingdomManager getKingdomManager() {
		return this.kingdomManager;
	}

	public StatusManager getStatusManager() {
		return this.statusManager;
	}

	public EconomyManager getEconomyManager() {
		return this.economyManager;
	}

	public ScoreboardManager getScoreboardManager() {
		return this.scoreboardManager;
	}

	public ChatManager getChatManager() {
		return this.chatManager;
	}

	public ShopManager getShopManager() {
		return this.shopManager;
	}

	public TitleManager getTitleManager() {
		return this.titleManager;
	}

	public CustomDamageManager getCustomDamageManager() {
		return this.customDamageManager;
	}

	public ClassManager getClassManager() {
		return this.classManager;
	}

	public MobManager getMobManager() {
		return this.mobManager;
	}
}
