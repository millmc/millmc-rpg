package com.millmc.title;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.millmc.title.interfaces.Title;
import com.millmc.title.interfaces.TitleAction;
import com.millmc.title.interfaces.TitleType;
import com.millmcapi.spigot.item.Item;

public class TitleInventory implements Listener {

	public ArrayList<Inventory> pages = new ArrayList<Inventory>();
	public UUID id;
	public int currpage = 0;
	public static HashMap<UUID, TitleInventory> users = new HashMap<UUID, TitleInventory>();
	public static final String nextPageName = "§aPróxima página";
	public static final String previousPageName = "§cPágina anterior";
	public static HashMap<UUID, Long> usersDelay = new HashMap<UUID, Long>();
	String name;
	private ItemStack main;
	private ItemStack nextpage;
	private TitleType type;

	public String getName() {
		return name;
	}

	public TitleInventory(List<ItemStack> items, String name, Player player, TitleType type) {
		this.type = type;
		this.id = UUID.randomUUID();

		Inventory page = getBlankPage(name);
		Integer slot = 10;
		for (int i = 0; i < items.size(); i++) {
			if (slot == 44) {
				this.pages.add(page);
				page = getBlankPage(name);
				slot = 10;
				page.setItem(slot, items.get(i));
			} else {
				page.setItem(slot, items.get(i));
				if (slot == 16 || slot == 25 || slot == 34) {
					slot += 3;
				} else {
					slot += 1;
				}
			}
		}
		this.name = name;
		this.pages.add(page);

		player.openInventory(this.pages.get(this.currpage));
		users.put(player.getUniqueId(), this);
	}

	private Inventory getBlankPage(String name) {
		Inventory page = Bukkit.createInventory(null, 54, name + " §7- Página " + this.currpage);

		Item has = new Item(Material.BOOK_AND_QUILL).setDisplayName("§aTitulos adquiridos");
		has.setLore("§7Clique para acessar a página", "§7com os títulos que você possui!");
		if (type == TitleType.HAS) {
			has.addFakeEnchant();
		}
		page.setItem(3, has);
		Item noHas = new Item(Material.BARRIER).setDisplayName("§aTitulos não adquiridos");
		noHas.setLore("§7Clique para acessar a página", "§7com os títulos que você não possui!");
		if (type == TitleType.NO_HAS) {
			noHas.addFakeEnchant();
		}
		page.setItem(5, noHas);

		this.nextpage = new ItemStack(Material.INK_SACK, 1, (short) 10);
		ItemMeta meta = nextpage.getItemMeta();
		meta.setDisplayName(nextPageName);
		nextpage.setItemMeta(meta);

		this.main = new ItemStack(Material.INK_SACK, 1, (short) 8);
		meta = main.getItemMeta();
		meta.setDisplayName(previousPageName);
		main.setItemMeta(meta);

		page.setItem(45, main);
		page.setItem(53, nextpage);
		return page;
	}

	public TitleInventory(InventoryClickEvent event, TitleManager titleManager) {
		Player player = (Player) event.getWhoClicked();

		if (event.getInventory().getName().startsWith("§7Titulos")) {

			this.nextpage = new ItemStack(Material.ARROW, 1);
			ItemMeta meta = nextpage.getItemMeta();
			meta.setDisplayName(nextPageName);
			nextpage.setItemMeta(meta);

			this.main = new ItemStack(Material.ARROW, 1);
			meta = main.getItemMeta();
			meta.setDisplayName(previousPageName);
			main.setItemMeta(meta);

			if (event.getClick() == ClickType.NUMBER_KEY) {
				event.setCancelled(true);
			}

			if (event.getCurrentItem() != null) {
				event.setCancelled(true);

				TitleInventory inv = TitleInventory.users.get(player.getUniqueId());

				if (event.getCurrentItem().hasItemMeta()) {
					String displayName = event.getCurrentItem().getItemMeta().getDisplayName();

					if (displayName != null) {

						if (displayName.equalsIgnoreCase(previousPageName)) {
							int pag = inv.currpage;
							if (pag - 1 < 0) {
								player.performCommand("titulos");
							} else {
								inv.currpage -= 1;
								if (inv.pages.get(inv.currpage) != null) {

									Inventory invs = Bukkit.createInventory(null, 6 * 9, inv.getName() + " §7- Página " + inv.currpage + "");

									invs.setContents(inv.pages.get(inv.currpage).getContents());

									invs.setItem(45, main);
									invs.setItem(53, nextpage);

									player.openInventory(invs);
								}
							}
						}

						if (displayName.equalsIgnoreCase(nextPageName)) {
							if (inv.currpage >= inv.pages.size() - 1) {
							} else {
								inv.currpage += 1;

								Inventory invs = Bukkit.createInventory(null, 6 * 9, inv.getName() + " §7- Página " + inv.currpage + "");

								invs.setContents(inv.pages.get(inv.currpage).getContents());

								invs.setItem(45, main);
								invs.setItem(53, nextpage);

								player.openInventory(invs);
							}
						}
					}

					if (event.getCurrentItem().getItemMeta().hasLore() && event.getCurrentItem().getItemMeta().getLore().size() >= 2) {
						Item item = new Item(event.getCurrentItem());

						if (event.getRawSlot() == 5 || event.getRawSlot() == 3) {
							if (event.getRawSlot() == 3) {
								player.performCommand("titulos has");
							} else {
								player.performCommand("titulos nohas");
							}
							return;
						}

						if (!item.hasKey("titleAction")) {
							return;
						}

						TitleAction action = null;

						try {
							action = TitleAction.valueOf(item.getString("titleAction"));
						} catch (Exception e) {
						}

						if (action == null) {
							return;
						}

						if (action == TitleAction.SELECT) {
							int id = item.getInt("titleId");
							Title title = titleManager.getTitle(id);

							TitlePlayer titlePlayer = titleManager.getTitlePlayer(player);
							titlePlayer.setSelect(title);
							titleManager.update(titlePlayer);

							player.performCommand("titulos has");
						}
					}
				}
				if (TitleInventory.previousPageName != null) {
					event.setCancelled(true);
				}
			}
		}
	}

}
