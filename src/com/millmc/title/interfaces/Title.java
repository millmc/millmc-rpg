package com.millmc.title.interfaces;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.millmc.Main;
import com.millmc.title.TitleManager;
import com.millmc.title.TitlePlayer;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

public interface Title {

	public int getId();

	public String getName(int level);

	public String getSkullName();

	public Listener getListeners();

	public List<String> getLore(int level);

	public default void ownTitle(Player player, int id, int level) {
		TitleManager titleManager = Main.getInstance().getTitleManager();
		TitlePlayer titlePlayer = titleManager.getTitlePlayer(player);
		Title title = titleManager.getTitle(id);

		titlePlayer.addTitle(title, level);
		titleManager.update(titlePlayer);

		TextComponent tx1 = new TextComponent("§aVocê desbloqueou um novo titulo! ");
		TextComponent tx2 = new TextComponent("§7§o(Clique para ver)");
		tx2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/titulos"));

		player.spigot().sendMessage(tx1, tx2);
	}

	public default String formatRoman(int i) {
		if (i == 0) {
			return "0";
		} else if (i == 1) {
			return "I";
		} else if (i == 2) {
			return "II";
		} else if (i == 3) {
			return "III";
		} else if (i == 4) {
			return "IV";
		} else if (i == 5) {
			return "V";
		}
		return "?";
	}

}
