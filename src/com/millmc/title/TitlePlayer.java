package com.millmc.title;

import java.util.Map;
import java.util.Set;

import com.millmc.title.interfaces.Title;

public class TitlePlayer {

	private String name;
	private Map<Integer, Integer> titles;
	private int select;

	public TitlePlayer(String name, Map<Integer, Integer> titles, int select) {
		this.name = name;
		this.titles = titles;
		this.select = select;
	}

	public String getName() {
		return this.name;
	}

	public Set<Integer> getTitles() {
		return this.titles.keySet();
	}

	public void addTitle(Title title, int level) {
		titles.put(title.getId(), level);
	}

	public boolean hasTitle(Title title) {
		return titles.containsKey(title.getId());
	}

	public int getTitleUsing() {
		return select;
	}

	public boolean isSelect(int id) {
		return select == id;
	}

	public boolean isSelect(Title title) {
		return select == title.getId();
	}

	public int getLevel(Title title) {
		return titles.get(title.getId());
	}

	public void setSelect(Title title) {
		this.select = title.getId();
	}
}
