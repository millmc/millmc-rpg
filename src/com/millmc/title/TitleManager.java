package com.millmc.title;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.title.commands.CommandTitle;
import com.millmc.title.interfaces.Title;
import com.millmc.title.storage.TitleSQLStorage;
import com.millmc.title.storage.TitleStorage;
import com.millmc.title.titles.Assassin;
import com.millmc.title.titles.Killerpig;
import com.millmc.title.titles.Starter;

public class TitleManager {

	private TitleStorage storage;
	private Map<String, TitlePlayer> titlesPlayers;
	private Map<Integer, Title> titles;

	public TitleManager() {
		this.storage = new TitleSQLStorage(this);
		this.titlesPlayers = new HashMap<String, TitlePlayer>();

		this.titles = new HashMap<Integer, Title>();
		Set<Title> title = new HashSet<Title>();
		title.add(new Starter());
		title.add(new Assassin());
		title.add(new Killerpig());

		title.forEach(i -> {
			if (titles.containsKey(i.getId())) {
				System.out.println("[Titles] Error on try load title '" + i.getId() + ", " + i.getName(1) + " id already registered!");
			} else {
				titles.put(i.getId(), i);
			}
		});

		Main.getInstance().getMillAPI().getCommand().registerCommand(new CommandTitle(this));

		Bukkit.getPluginManager().registerEvents(new TitleListener(this), Main.getInstance());
	}

	public Map<Integer, Title> getTitles() {
		return this.titles;
	}

	public Title getTitle(int id) {
		return titles.get(id);
	}

	public Collection<TitlePlayer> getTitlePlayers() {
		return this.titlesPlayers.values();
	}

	public TitlePlayer getTitlePlayer(String name) {
		return titlesPlayers.get(name);
	}

	public TitlePlayer getTitlePlayer(Player player) {
		return getTitlePlayer(player.getName());
	}

	public void load(String name) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			if (!titlesPlayers.containsKey(name)) {
				TitlePlayer playerTitle = storage.download(name);

				if (playerTitle == null) {
					playerTitle = storage.add(name);
				}

				titlesPlayers.put(name, playerTitle);
			}
		});
	}

	public void unload(String name) {
		titlesPlayers.remove(name);
	}

	public void update(TitlePlayer titlePlayer) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			storage.update(titlePlayer);
		});
	}

}
