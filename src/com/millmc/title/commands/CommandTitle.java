package com.millmc.title.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.millmc.title.TitleInventory;
import com.millmc.title.TitleManager;
import com.millmc.title.TitlePlayer;
import com.millmc.title.interfaces.Title;
import com.millmc.title.interfaces.TitleAction;
import com.millmc.title.interfaces.TitleType;
import com.millmcapi.spigot.item.Item;

public class CommandTitle extends Command {

	private TitleManager titleManager;

	public CommandTitle(TitleManager titleManager) {
		super("titulos");
		this.titleManager = titleManager;
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		TitlePlayer titlePlayer = titleManager.getTitlePlayer(player);

		if (args.length == 0) {
			player.performCommand("titulos has");
		} else if (args.length >= 1) {

			if (args[0].equalsIgnoreCase("has")) {
				List<ItemStack> items = new ArrayList<ItemStack>();

				for (int x = 0; x < titleManager.getTitles().size(); x++) {
					Title title = titleManager.getTitle(x);

					if (titlePlayer.hasTitle(title)) {
						Item item = new Item(Material.SKULL_ITEM);
						item.setDurability((short) 3);
						item.setCustomSkull(title.getSkullName());
						int level = titlePlayer.getLevel(title);
						item.setDisplayName("§7Título: §a" + title.getName(level));
						List<String> lore = new ArrayList<String>();

						lore.addAll(title.getLore(level));

						if (titlePlayer.isSelect(title)) {
							lore.add("");
							lore.add("§eVocê está com esse título selecionado!");
							item.addString("titleAction", TitleAction.NOTHING.name());
							item.addInt("titleId", title.getId());
						} else {
							lore.add("");
							lore.add("§eClique para selecionar!");
							item.addString("titleAction", TitleAction.SELECT.name());
							item.addInt("titleId", title.getId());
						}

						item.setLore(lore);
						items.add(item);
					}
				}

				new TitleInventory(items, "§7Titulos", player, TitleType.HAS);
				return false;
			}

			if (args[0].equalsIgnoreCase("nohas")) {
				List<ItemStack> items = new ArrayList<ItemStack>();

				for (int x = 0; x < titleManager.getTitles().size(); x++) {
					Title title = titleManager.getTitle(x);

					if (!titlePlayer.hasTitle(title)) {
						Item item = new Item(Material.SKULL_ITEM);
						item.setDurability((short) 3);
						item.setCustomSkull(title.getSkullName());
						item.setDisplayName("§7Título: §a" + title.getName(1));
						List<String> lore = new ArrayList<String>();

						lore.addAll(title.getLore(0));

						lore.add("");
						lore.add("§cVocê não possui este título!");
						item.addString("titleAction", TitleAction.NOTHING.name());

						item.setLore(lore);
						items.add(item);
					}
				}

				new TitleInventory(items, "§7Titulos", player, TitleType.NO_HAS);
				return false;
			}

		}

		return false;
	}

}
