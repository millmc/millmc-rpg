package com.millmc.title;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.millmcapi.general.users.custom.UserJoinEvent;
import com.millmcapi.general.users.custom.UserQuitEvent;

public class TitleListener implements Listener {

	private TitleManager titleManager;

	public TitleListener(TitleManager titleManager) {
		this.titleManager = titleManager;
	}

	@EventHandler
	public void onUserJoin(UserJoinEvent event) {
		Player player = event.getPlayer();
		titleManager.load(player.getName());
	}

	@EventHandler
	public void onUserQuit(UserQuitEvent event) {
		Player player = event.getPlayer();
		titleManager.unload(player.getName());
	}

	@EventHandler
	public void onInv(InventoryClickEvent event) {
		new TitleInventory(event, titleManager);
	}

}
