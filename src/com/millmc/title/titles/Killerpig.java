package com.millmc.title.titles;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import com.millmc.Main;
import com.millmc.title.interfaces.Title;

public class Killerpig implements Title {

	public Killerpig() {
		Bukkit.getPluginManager().registerEvents(getListeners(), Main.getInstance());
	}

	@Override
	public int getId() {
		return 2;
	}

	@Override
	public String getName(int level) {
		return "Matador de porcos " + formatRoman(level);
	}

	@Override
	public String getSkullName() {
		return "1092381293821938123098123";
	}

	@Override
	public Listener getListeners() {
		return new Listener() {
		};
	}

	@Override
	public List<String> getLore(int level) {
		List<String> lore = new ArrayList<String>();
		if (level == 0) {
			lore.add("§7Mate");
		} else if (level == 1) {
			lore.add("§7Mate");
		} else if (level == 2) {
			lore.add("§7Mate");
		} else {
			lore.add("§7Mate");
		}
		return lore;
	}

}
