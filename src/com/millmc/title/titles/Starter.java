package com.millmc.title.titles;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.event.Listener;

import com.millmc.title.interfaces.Title;

public class Starter implements Title {

	@Override
	public int getId() {
		return 0;
	}

	@Override
	public String getName(int level) {
		return "Iniciante";
	}

	@Override
	public String getSkullName() {
		return "219e36a87baf0ac76314352f59a7f63bdb3f4c86bd9bba6927772c01d4d1";
	}

	@Override
	public Listener getListeners() {
		return null;
	}

	@Override
	public List<String> getLore(int level) {
		List<String> lore = new ArrayList<String>();
		lore.add("§7Você desbloqueou esta conquista após entrar");
		lore.add("§7pela primeira vez em nosso servidor!");
		return lore;
	}

}
