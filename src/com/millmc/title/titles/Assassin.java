package com.millmc.title.titles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.millmc.Main;
import com.millmc.title.interfaces.Title;

public class Assassin implements Title {

	public Assassin() {
		Bukkit.getPluginManager().registerEvents(getListeners(), Main.getInstance());
	}

	@Override
	public int getId() {
		return 1;
	}

	@Override
	public String getName(int level) {
		return "Assassino " + formatRoman(level);
	}

	@Override
	public String getSkullName() {
		return "daa4e2294df370b9a50cb924cdda78f740b0fbaf5a687106178505c80a79addc";
	}

	private Map<String, Integer> countKills = new HashMap<String, Integer>();

	@Override
	public Listener getListeners() {
		return new Listener() {

			@EventHandler
			public void onDeath(PlayerDeathEvent event) {
				Player player = event.getEntity();
				Player killer = player.getKiller();

				if (killer != null) {
					int kills = 1;
					if (countKills.containsKey(killer.getName())) {
						kills = countKills.get(killer.getName()) + 1;
					}
					countKills.put(killer.getName(), kills);

					if (kills == 1) {
						ownTitle(killer, getId(), 1);
					} else if (kills == 2) {
						ownTitle(killer, getId(), 2);
					} else if (kills == 3) {
						ownTitle(killer, getId(), 3);
					}
				}
			}
		};
	}

	@Override
	public List<String> getLore(int level) {
		List<String> lore = new ArrayList<String>();
		if (level == 0) {
			lore.add("§7Para desbloquear esse título é necessário");
			lore.add("§7eliminar 1 jogador!");
		} else if (level == 1) {
			lore.add("§7Para desbloquear o nível II");
			lore.add("§7você precisa matar 2 jogadores!");
		} else if (level == 2) {
			lore.add("§7Para desbloquear o nível III");
			lore.add("§7você precisa matar 3 jogadores!");
		} else {
			lore.add("§7Você atingiu o nível máximo");
			lore.add("§7agora você é um assassino experiente!");
		}
		return lore;
	}
}
