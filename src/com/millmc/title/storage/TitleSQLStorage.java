package com.millmc.title.storage;

import java.util.HashMap;

import com.millmc.title.TitleManager;
import com.millmc.title.TitlePlayer;

public class TitleSQLStorage implements TitleStorage {

	private TitleManager titleManager;

	public TitleSQLStorage(TitleManager titleManager) {
		this.titleManager = titleManager;
	}

	@Override
	public TitlePlayer download(String name) {
		TitlePlayer titlePlayer = new TitlePlayer(name, new HashMap<Integer, Integer>(), 0);
		titlePlayer.addTitle(titleManager.getTitle(0), 1);
		return titlePlayer;
	}

	@Override
	public TitlePlayer add(String name) {
		TitlePlayer titlePlayer = new TitlePlayer(name, new HashMap<Integer, Integer>(), 0);
		titlePlayer.addTitle(titleManager.getTitle(0), 1);
		return titlePlayer;
	}

	@Override
	public void update(TitlePlayer playerTitle) {
	}

}
