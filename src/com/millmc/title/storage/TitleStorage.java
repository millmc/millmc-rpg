package com.millmc.title.storage;

import com.millmc.title.TitlePlayer;

public interface TitleStorage {

	public TitlePlayer download(String name);

	public TitlePlayer add(String name);

	public void update(TitlePlayer playerTitle);

}
