package com.millmc.mobs.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.mobs.MobBoss;
import com.millmc.mobs.MobManager;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandMonsters extends Command {

	private UserManager userManager;
	private GroupManager groupManager;
	private MobManager mobManager;

	public CommandMonsters(MobManager mobManager) {
		super("monsters");
		this.mobManager = mobManager;
		Module module = Main.getInstance().getMillAPI().getModule();
		this.userManager = module.getUserManager();
		this.groupManager = module.getGroupManager();
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player);

		if (!groupManager.check(user.getGroupId(), 90)) {
			player.sendMessage("§cVocê não tem acesso a esse comando!");
			return false;
		}

		if (args.length == 0) {

			return false;
		} else if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("set")) {
				if (args.length <= 3) {
					player.sendMessage("§7Use o comando §a/" + arg1 + " set <boss type> <minions type> <level>");
					return false;
				}

				EntityType boss = null;
				EntityType minions = null;

				try {
					boss = EntityType.valueOf(args[1].toUpperCase());
					minions = EntityType.valueOf(args[2].toUpperCase());
				} catch (Exception e) {
				}

				if (boss == null) {
					player.sendMessage("§cNão foi encontrado um boss do tipo " + args[1]);
					return false;
				}

				if (minions == null) {
					player.sendMessage("§cNão foi encontrado um minion do tipo " + args[1]);
					return false;
				}

				int level = 0;

				try {
					level = Integer.parseInt(args[3]);
				} catch (NumberFormatException e) {
					player.sendMessage("§cUse apenas números!");
					return false;
				}

				MobBoss mobBoss = new MobBoss(player.getLocation(), boss, minions, level);
				mobBoss.reset();
				mobManager.registerBoss(mobBoss);

				player.sendMessage("§7Localizações para bosses criada!");
				return false;
			}
		}

		return false;
	}

}
