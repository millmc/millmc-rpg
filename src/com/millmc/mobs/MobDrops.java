package com.millmc.mobs;

import org.bukkit.inventory.ItemStack;

public class MobDrops {

	private ItemStack itemStack;
	private int minDrops;
	private int maxDrops;
	private double chance;

	public MobDrops(ItemStack itemStack, int minDrops, int maxDrops, double chance) {
		this.itemStack = itemStack;
		this.minDrops = minDrops;
		this.maxDrops = maxDrops;
		this.chance = chance;
	}

	public ItemStack getItemStack() {
		return this.itemStack.clone();
	}

	public int getMinDrops() {
		return this.minDrops;
	}

	public int getMaxDrops() {
		return this.maxDrops;
	}

	public double getChance() {
		return this.chance;
	}

}
