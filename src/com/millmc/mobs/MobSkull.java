package com.millmc.mobs;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.millmcapi.spigot.item.Item;

public enum MobSkull {

	CREEPER("Creeper", "f4254838c33ea227ffca223dddaabfe0b0215f70da649e944477f44370ca6952"),
	SKELETON("Esqueleto", "301268e9c492da1f0d88271cb492a4b302395f515a7bbf77f4a20b95fc02eb2"),
	SPIDER("Aranha", "cd541541daaff50896cd258bdbdd4cf80c3ba816735726078bfe393927e57f1"),
	ZOMBIE("Zumbi", "56fc854bb84cf4b7697297973e02b79bc10698460b51a639c60e5e417734e11"),
	SLIME("Slime", "a20e84d32d1e9c919d3fdbb53f2b37ba274c121c57b2810e5a472f40dacf004f"),
	GHAST("Ghast", "8b6a72138d69fbbd2fea3fa251cabd87152e4f1c97e5f986bf685571db3cc0"),
	PIG_ZOMBIE("Homem-porco zumbi", "74e9c6e98582ffd8ff8feb3322cd1849c43fb16b158abb11ca7b42eda7743eb"),
	ENDERMAN("Enderman", "7a59bb0a7a32965b3d90d8eafa899d1835f424509eadd4e6b709ada50b9cf"),
	CAVE_SPIDER("Aranha da caverna", "41645dfd77d09923107b3496e94eeb5c30329f97efc96ed76e226e98224"),
	SILVERFISH("Traça", "da91dab8391af5fda54acd2c0b18fbd819b865e1a8f1d623813fa761e924540"),
	BLAZE("Blaze", "b78ef2e4cf2c41a2d14bfde9caff10219f5b1bf5b35a49eb51c6467882cb5f0"),
	MAGMA_CUBE("Cubo de magma", "38957d5023c937c4c41aa2412d43410bda23cf79a9f6ab36b76fef2d7c429"),
	ENDER_DRAGON("Ender dragon", ""),
	WITHER("Wither", "cdf74e323ed41436965f5c57ddf2815d5332fe999e68fbb9d6cf5c8bd4139f"),
	BAT("Morcego", "9e99deef919db66ac2bd28d6302756ccd57c7f8b12b9dca8f41c3e0a04ac1cc"),
	WITCH("Bruxa", "20e13d18474fc94ed55aeb7069566e4687d773dac16f4c3f8722fc95bf9f2dfa"),
	ENDERMITE("Endermite", "5bc7b9d36fb92b6bf292be73d32c6c5b0ecc25b44323a541fae1f1e67e393a3e"),
	GUARDIAN("Guardian", "a0bf34a71e7715b6ba52d5dd1bae5cb85f773dc9b0d457b4bfc5f9dd3cc7c94"),
	PIG("Porco", "621668ef7cb79dd9c22ce3d1f3f4cb6e2559893b6df4a469514e667c16aa4"),
	SHEEP("Ovelha", "f31f9ccc6b3e32ecf13b8a11ac29cd33d18c95fc73db8a66c5d657ccb8be70"),
	COW("Vaca", "5d6c6eda942f7f5f71c3161c7306f4aed307d82895f9d2b07ab4525718edc5"),
	CHICKEN("Galinha", "1638469a599ceef7207537603248a9ab11ff591fd378bea4735b346a7fae893"),
	SQUID("Lula", "01433be242366af126da434b8735df1eb5b3cb2cede39145974e9c483607bac"),
	WOLF("Lobo", "69d1d3113ec43ac2961dd59f28175fb4718873c6c448dfca8722317d67"),
	MUSHROOM_COW("Coguvaca", "d0bc61b9757a7b83e03cd2507a2157913c2cf016e7c096a4d6cf1fe1b8db"),
	SNOWMAN("Boneco de neve", "ee28a03ed2eb90eaff1a119a5b554452701b97af47bff73ce710849c6b0"),
	OCELOT("Ocelot", "5657cd5c2989ff97570fec4ddcdc6926a68a3393250c1be1f0b114a1db1"),
	IRON_GOLEM("Iron golem", "89091d79ea0f59ef7ef94d7bba6e5f17f2f7d4572c44f90f76c4819a714"),
	HORSE("Cavalo", "628d1ab4be1e28b7b461fdea46381ac363a7e5c3591c9e5d2683fbe1ec9fcd3"),
	RABBIT("Coelho", "7d1169b2694a6aba826360992365bcda5a10c89a3aa2b48c438531dd8685c3a7"),
	VILLAGER("Villager", "822d8e751c8f2fd4c8942c44bdb2f5ca4d8ae8e575ed3eb34c18a86e93b");

	private String skullURL;
	private String name;

	private MobSkull(String name, String skullURL) {
		this.name = name;
		this.skullURL = skullURL;
	}

	public String getSkullURL() {
		return this.skullURL;
	}

	public String getName() {
		return this.name;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack getItemStack(EntityType entityType) {
		Item item = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3);

		item.setDisplayName("§7Cabeça de §a" + entityType.getName());

		MobSkull mobSkull = MobSkull.valueOf(entityType.toString());
		if (mobSkull != null) {
			item.setCustomSkull2(mobSkull.getSkullURL());
			item.setDisplayName("§7Cabeça de §a" + mobSkull.getName());
		}

		item.setLore("§7Utilize para trocar com os npc's");
		return item;
	}
}
