package com.millmc.mobs;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.entity.Animals;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.ItemStack;

import com.millmc.Main;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.event.update.UpdateEvent;
import com.millmcapi.spigot.event.update.UpdateType;
import com.millmcapi.spigot.utils.MetadataUtils;

public class MobListener implements Listener {

	private MobManager mobManager;
	private UserManager userManager;

	public MobListener(MobManager mobManager) {
		this.mobManager = mobManager;
		this.userManager = Main.getInstance().getMillAPI().getModule().getUserManager();
	}

	@EventHandler
	public void onTime(UpdateEvent event) {
		if (event.getUpdateType() == UpdateType.SEC) {
			Set<MobBoss> queued = new HashSet<MobBoss>(mobManager.getQueued());
			queued.forEach(i -> {
				if (i.canReset()) {
					i.respawnMonsters();
					mobManager.getQueued().remove(i);
				}
			});
		}
	}

	@EventHandler
	public void onSpawn(EntitySpawnEvent event) {
		if (event.getEntity() instanceof Animals) {

			Animals animals = (Animals) event.getEntity();
			MobInfos mobInfos = mobManager.getMobs().get(animals.getType());

			int min = 1;
			int max = 1;

			if (mobInfos != null) {
				min = mobInfos.getMinLevel();
				max = mobInfos.getMaxLevel();
			}

			int level = ThreadLocalRandom.current().nextInt(min, max);

			MetadataUtils.setMetadata(animals, "level", level);
			animals.setCustomNameVisible(true);
			animals.setCustomName("§7Level: §a" + level);

		} else if (event.getEntity() instanceof Monster) {
			Monster monster = (Monster) event.getEntity();

			if (MetadataUtils.hasMetadata(monster, "minions")) {
				int max = mobManager.getLevelByAnimal(monster);
				int min = (int) ((double) max * 0.3);
				int level = ThreadLocalRandom.current().nextInt(min, max);

				MetadataUtils.setMetadata(monster, "level", level);
				monster.setCustomNameVisible(true);
				monster.setCustomName("§7Level: §a" + level);

			} else if (MetadataUtils.hasMetadata(monster, "miniboss")) {
				int level = mobManager.getLevelByAnimal(monster);

				MetadataUtils.setMetadata(monster, "level", level);
				monster.setCustomNameVisible(true);
				monster.setCustomName("§7Level: §a" + level);

				int health = 40 * level;
				monster.setMaxHealth(health);
				monster.setHealth(health);
			}
		}
	}

	@EventHandler
	public void onDeath(EntityDeathEvent event) {
		LivingEntity entity = event.getEntity();

		if (entity instanceof Player) {
			return;
		}

		int level = mobManager.getLevelByAnimal(entity);
		double exp = mobManager.getExp(entity.getType(), level);

		if (exp > 0.0000) {
			event.getDrops().clear();
			event.setDroppedExp(0);

			MobInfos mobInfos = mobManager.getMobs().get(entity.getType());
			if (mobInfos != null) {

				for (MobDrops mobDrops : mobInfos.getDrops()) {
					ItemStack itemStack = mobDrops.getItemStack().clone();
					int r = ThreadLocalRandom.current().nextInt(0, 100);
					if (r <= mobDrops.getChance()) {
						int drops = ThreadLocalRandom.current().nextInt(mobDrops.getMinDrops(), mobDrops.getMaxDrops() + 1);

						itemStack.setAmount(drops);

						event.getDrops().add(itemStack);
					}
				}
			}

			Player killer = entity.getKiller();
			if (killer != null) {
				User user = userManager.getUser(killer.getName());
				user.addExp(exp);
				userManager.update(user);
			}
		}
	}
}
