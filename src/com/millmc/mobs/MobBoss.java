package com.millmc.mobs;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import com.millmcapi.spigot.utils.MetadataUtils;

public class MobBoss {

	private Location location;
	private EntityType boss;
	private EntityType minions;

	private long time;
	private boolean reset;
	private int level;

	private Set<String> spawnedMinions;

	public MobBoss(Location location, EntityType boss, EntityType minions, int level) {
		this.location = location;
		this.boss = boss;
		this.minions = minions;
		this.level = level;

		this.time = 0l;
		this.reset = false;

		this.spawnedMinions = new HashSet<String>();
	}

	public Location getLocation() {
		return this.location;
	}

	public EntityType getBoss() {
		return this.boss;
	}

	public EntityType getMinions() {
		return this.minions;
	}

	public void reset() {
		// this.time = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10);

		this.time = System.currentTimeMillis() + (10 * 1000);
		this.reset = true;
	}

	public boolean canReset() {
		if (reset && System.currentTimeMillis() >= time) {
			this.reset = false;
			return true;
		}
		return false;
	}

	private void spawnBoss() {
		Location loc = location.clone();
		loc.setY(loc.getWorld().getHighestBlockYAt(loc));

		Entity entity = loc.getWorld().spawn(loc, minions.getEntityClass());
		MetadataUtils.setMetadata(entity, "miniboss", true);
		MetadataUtils.setMetadata(entity, "level", level);
	}

	public void respawnMonsters() {
		int radius = 10;
		for (int x = -radius; x < radius; x++) {
			for (int z = -radius; z < radius; z++) {
				if (x + z % 2 == 0) {
					Location loc = location.clone().add(x, 0, z);
					loc.setY(loc.getWorld().getHighestBlockYAt(loc));

					Entity entity = loc.getWorld().spawn(loc, minions.getEntityClass());
					MetadataUtils.setMetadata(entity, "minions", true);
					MetadataUtils.setMetadata(entity, "level", level - (level * 0.2));
					spawnedMinions.add(entity.getUniqueId().toString());
				}
			}
		}
		spawnBoss();
	}
}
