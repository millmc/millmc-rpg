package com.millmc.mobs;

import org.bukkit.entity.EntityType;

public class MobInfos {

	private EntityType type;
	private int minLevel;
	private int maxLevel;
	private double expPerLevel;
	private MobDrops[] drops;

	public MobInfos(EntityType type, int minLevel, int maxLevel, double expPerLevel, MobDrops[] drops) {
		this.type = type;
		this.minLevel = minLevel;
		this.maxLevel = maxLevel;
		this.expPerLevel = expPerLevel;
		this.drops = drops;
	}

	public EntityType getType() {
		return this.type;
	}

	public int getMinLevel() {
		return this.minLevel;
	}

	public int getMaxLevel() {
		return this.maxLevel + 1;
	}

	public double getExpPerLevel() {
		return this.expPerLevel;
	}

	public double getExp(int level) {
		return expPerLevel * (level <= 0 ? 1.0 : level);
	}

	public MobDrops[] getDrops() {
		return this.drops;
	}

}
