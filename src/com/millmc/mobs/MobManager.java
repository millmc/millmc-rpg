package com.millmc.mobs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import com.millmc.Main;
import com.millmc.mobs.commands.CommandMonsters;
import com.millmcapi.spigot.item.Item;
import com.millmcapi.spigot.utils.MetadataUtils;

public class MobManager {

	private Map<EntityType, MobInfos> mobs;
	private Set<MobBoss> bosses;
	private Set<MobBoss> queued;

	public MobManager() {
		this.mobs = new HashMap<EntityType, MobInfos>();
		this.bosses = new HashSet<MobBoss>();
		this.queued = new HashSet<MobBoss>();

		mobs.put(EntityType.CHICKEN, new MobInfos(EntityType.CHICKEN, 1, 3, 0.3,
				new MobDrops[] { new MobDrops(new Item(Material.EXP_BOTTLE), 1, 3, 20), new MobDrops(MobSkull.getItemStack(EntityType.CHICKEN), 1, 1, 15) }));
		mobs.put(EntityType.RABBIT, new MobInfos(EntityType.RABBIT, 2, 4, 0.3,
				new MobDrops[] { new MobDrops(new Item(Material.EXP_BOTTLE), 1, 3, 20), new MobDrops(MobSkull.getItemStack(EntityType.RABBIT), 1, 1, 15) }));
		mobs.put(EntityType.PIG, new MobInfos(EntityType.PIG, 2, 4, 0.3,
				new MobDrops[] { new MobDrops(new Item(Material.EXP_BOTTLE), 1, 3, 20), new MobDrops(MobSkull.getItemStack(EntityType.PIG), 1, 1, 15) }));
		mobs.put(EntityType.SHEEP, new MobInfos(EntityType.SHEEP, 3, 6, 0.3,
				new MobDrops[] { new MobDrops(new Item(Material.EXP_BOTTLE), 1, 3, 20), new MobDrops(MobSkull.getItemStack(EntityType.SHEEP), 1, 1, 15) }));
		mobs.put(EntityType.COW, new MobInfos(EntityType.COW, 4, 7, 0.3,
				new MobDrops[] { new MobDrops(new Item(Material.EXP_BOTTLE), 1, 3, 20), new MobDrops(MobSkull.getItemStack(EntityType.COW), 1, 1, 15) }));

		Main.getInstance().getMillAPI().getCommand().registerCommand(new CommandMonsters(this));

		Bukkit.getPluginManager().registerEvents(new MobListener(this), Main.getInstance());
	}

	public Map<EntityType, MobInfos> getMobs() {
		return this.mobs;
	}

	public Double getExp(EntityType type, int level) {
		return mobs.containsKey(type) ? mobs.get(type).getExp(level) : 0.0;
	}

	public int getLevelByAnimal(LivingEntity entity) {
		if (MetadataUtils.hasMetadata(entity, "level")) {
			return MetadataUtils.getMetadataAsInt(entity, "level");
		}
		return 1;
	}

	public void registerBoss(MobBoss mobBoss) {
		bosses.add(mobBoss);
	}

	public Set<MobBoss> getBosses() {
		return this.bosses;
	}

	public Set<MobBoss> getQueued() {
		return this.queued;
	}
}
