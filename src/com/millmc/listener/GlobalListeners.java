package com.millmc.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.millmc.Main;
import com.millmcapi.general.format.FormatUtils;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.custom.PlayerExpChangeEvent;
import com.millmcapi.general.users.custom.PlayerLevelChangeEvent;
import com.millmcapi.general.users.custom.UserJoinEvent;
import com.millmcapi.spigot.utils.TitleEvents;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class GlobalListeners implements Listener {

	@EventHandler
	public void onUserJoin(UserJoinEvent event) {
		Player player = event.getPlayer();
		User user = event.getUser();

		if (user.getClassName().equalsIgnoreCase("nothing")) {
			player.teleport(new Location(Bukkit.getWorld("world"), -531, 7, 697));
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		event.setQuitMessage(null);
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		event.setDeathMessage(null);

		Bukkit.getScheduler().runTask(Main.getInstance(), () -> {
			event.getEntity().spigot().respawn();
		});
	}

	@EventHandler
	public void onExp(PlayerExpChangeEvent event) {
		Player player = event.getPlayer();
		TitleEvents.sendTitleActionBar(player, "§7Você recebeu §a" + FormatUtils.format(event.getWon()) + " §7experiências!");
	}

	@EventHandler
	public void onExp(PlayerLevelChangeEvent event) {
		Player player = event.getPlayer();
		User user = event.getUser();

		player.sendMessage("");
		player.sendMessage("§7Parabéns, você atingiu o level §a" + FormatUtils.format2(user.getLevel()) + "§7!");
		player.sendMessage("§7Você recebeu §a1 ponto(s) §7de atribuição!");

		TextComponent tx = new TextComponent("§7Use §a/status §7para melhorar suas habilidades! ");
		TextComponent tx2 = new TextComponent("§7§o(Clique aqui)");
		tx2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/status"));
		player.spigot().sendMessage(tx, tx2);

		player.sendMessage("");
	}
}
