package com.millmc.status;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.millmc.Main;
import com.millmc.status.constructors.Stats;
import com.millmc.status.custom.StatusUpgradeEvent;
import com.millmcapi.general.format.FormatUtils;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.item.Item;
import com.millmcapi.spigot.skull.SkullData;

public class StatusInventory implements Listener {

	private StatusManager statusManager;
	private UserManager userManager;

	private Item damageItem;
	private Item blockItem;
	private Item healthItem;
	private Item criticalItem;
	private Item chanceCriticalItem;

	public StatusInventory(StatusManager statusManager, UserManager userManager) {
		this.statusManager = statusManager;
		this.userManager = userManager;

		this.damageItem = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setCustomSkull2(Stats.DAMAGE.getSkull());
		this.blockItem = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setCustomSkull2(Stats.BLOCK.getSkull());
		this.healthItem = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setCustomSkull2(Stats.HEALTH.getSkull());
		this.criticalItem = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setCustomSkull2(Stats.CRITICAL.getSkull());
		this.chanceCriticalItem = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setCustomSkull2(Stats.CRITICAL_CHANCE.getSkull());

		Bukkit.getPluginManager().registerEvents(this, Main.getInstance());
	}

	public void openMenu(Player player) {
		Status status = statusManager.getStatus(player);
		User user = userManager.getUser(player.getName());

		Inventory inv = Bukkit.createInventory(null, 5 * 9, "§7Status");

		inv.setItem(4, new Item(Material.BOOK_AND_QUILL).setDisplayName("§7Você possui §a" + FormatUtils.format2(user.getPoints()) + "§7 pontos.")
				.setLore("§7Você pode usar esses pontos para", "§7evoluir o nível de cada status."));

		Item arrowUp = new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setCustomSkull2(SkullData.OAK_WOOD_ARROW_UP.getValue());

		int levelDamage = status.getLevelStats(Stats.DAMAGE);
		int levelBlock = status.getLevelStats(Stats.BLOCK);
		int levelHealth = status.getLevelStats(Stats.HEALTH);
		int levelCrit = status.getLevelStats(Stats.CRITICAL);
		int levelChanceCrit = status.getLevelStats(Stats.CRITICAL_CHANCE);

		inv.setItem(19,
				new Item(damageItem.clone()).setDisplayName("§7Status: §aDano").setLore("§7Dano atual: §a" + FormatUtils.format(status.getDamage(levelDamage)),
						"§7Dano ao evoluir: §a" + FormatUtils.format(status.getDamage(levelDamage + 1)), "§7",
						"§7Pontos atribuidos: §a" + FormatUtils.format2(levelDamage)));
		inv.setItem(28, new Item(arrowUp.clone()).setDisplayName("§7Clique para evoluir.").addString("stats", Stats.DAMAGE.name()));

		inv.setItem(20,
				new Item(blockItem.clone()).setDisplayName("§7Status: §aBloquear").setLore(
						"§7Chance de bloqueio atual: §a" + FormatUtils.format(status.getChanceBlock(levelBlock)) + "%",
						"§7Chance de bloqueio ao evoluir: §a" + FormatUtils.format(status.getChanceBlock(levelBlock + 1)) + "%", "§7",
						"§7Pontos atribuidos: §a" + FormatUtils.format2(levelBlock)));
		inv.setItem(29, new Item(arrowUp.clone()).setDisplayName("§7Clique para evoluir.").addString("stats", Stats.BLOCK.name()));

		inv.setItem(22,
				new Item(healthItem.clone()).setDisplayName("§7Status: §aVida").setLore("§7Vida atual: §a" + FormatUtils.format(status.getMaxHealth(levelHealth)),
						"§7Vida ao evoluir: §a" + FormatUtils.format(status.getMaxHealth(levelHealth + 1)), "§7",
						"§7Pontos atribuidos: §a" + FormatUtils.format2(levelHealth)));
		inv.setItem(31, new Item(arrowUp.clone()).setDisplayName("§7Clique para evoluir.").addString("stats", Stats.HEALTH.name()));

		inv.setItem(24,
				new Item(criticalItem.clone()).setDisplayName("§7Status: §aCrítico").setLore(
						"§7Dano crítico atual: §a" + FormatUtils.format(status.getDamageCrit(levelCrit)),
						"§7Dano crítico ao evoluir: §a" + FormatUtils.format(status.getDamageCrit(levelCrit + 1)), "§7",
						"§7Pontos atribuidos: §a" + FormatUtils.format2(levelCrit)));
		inv.setItem(33, new Item(arrowUp.clone()).setDisplayName("§7Clique para evoluir.").addString("stats", Stats.CRITICAL.name()));

		inv.setItem(25,
				new Item(chanceCriticalItem.clone()).setDisplayName("§7Status: §aChance Crítica").setLore(
						"§7Chance crítica atual: §a" + FormatUtils.format(status.getChanceCrit(levelChanceCrit)) + "%",
						"§7Chance crítica ao evoluir: §a" + FormatUtils.format(status.getChanceCrit(levelChanceCrit + 1)) + "%", "§7",
						"§7Pontos atribuidos: §a" + FormatUtils.format2(levelChanceCrit)));
		inv.setItem(34, new Item(arrowUp.clone()).setDisplayName("§7Clique para evoluir.").addString("stats", Stats.CRITICAL_CHANCE.name()));

		player.openInventory(inv);
	}

	@EventHandler
	public void onInv(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();

		if (player.getOpenInventory() == null) {
			return;
		}

		if (!player.getOpenInventory().getTitle().equalsIgnoreCase("§7Status")) {
			return;
		}
		event.setCancelled(true);

		ItemStack itemStack = event.getCurrentItem();

		if (itemStack == null || itemStack.getType() == Material.AIR) {
			return;
		}

		Item item = new Item(itemStack);

		if (!item.hasKey("stats")) {
			return;
		}

		User user = userManager.getUser(player.getName());

		if (user.getPoints() <= 0) {
			player.sendMessage("§cVocê não possui pontos para atribuir!");
			return;
		}

		Stats stats = Stats.valueOf(item.getString("stats"));
		Status status = statusManager.getStatus(player);

		int levelStats = status.getLevelStats(stats);

		if (levelStats >= stats.getMaxlevel()) {
			player.sendMessage("§cVocê já atribuiu o máximo de pontos nesse status!");
			return;
		}

		status.addLevelStats(stats);
		user.removePoints(1);

		statusManager.update(status);
		userManager.update(user);

		player.sendMessage("§7Você atribuiu §a1 ponto(s) §7no status §a" + stats.getName());
		player.getWorld().playSound(player.getLocation(), Sound.LEVEL_UP, 1.3f, 1.3f);
		player.performCommand("status");

		Bukkit.getPluginManager().callEvent(new StatusUpgradeEvent(player, user, stats));
	}

}
