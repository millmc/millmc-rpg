package com.millmc.status.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.status.Status;
import com.millmc.status.StatusManager;
import com.millmc.status.constructors.Stats;
import com.millmc.status.custom.StatusUpgradeEvent;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandStatus extends Command {

	private StatusManager statusManager;
	private UserManager userManager;
	private GroupManager groupManager;

	public CommandStatus(StatusManager statusManager, UserManager userManager) {
		super("status");
		this.statusManager = statusManager;
		this.userManager = userManager;
		this.groupManager = Main.getInstance().getMillAPI().getModule().getGroupManager();
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player);

		if (args.length == 0) {
			statusManager.getStatusInventory().openMenu(player);
		} else if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("set")) {
				if (!groupManager.check(user.getGroupId(), 100)) {
					player.sendMessage("§cVocê não tem permissão!");
					return false;
				}

				if (args.length <= 3) {
					player.sendMessage("§7Use o comando: §a/" + arg1 + " set <player> <status> <level>");
					return false;
				}

				Player target = Bukkit.getPlayer(args[1]);

				if (target == null || !target.getName().equalsIgnoreCase(args[1])) {
					player.sendMessage("§cJogador não está conectado!");
					return false;
				}

				Stats status = null;
				try {
					status = Stats.valueOf(args[2].toUpperCase());
				} catch (Exception e) {
				}

				if (status == null) {
					player.sendMessage("§cEsse status não foi encontrado!");
					return false;
				}

				int level = 0;

				try {
					level = Integer.parseInt(args[3]);
				} catch (NumberFormatException e) {
					player.sendMessage("§cUse apenas números!");
					return false;
				}

				Status statusPlayer = statusManager.getStatus(target);

				if (statusPlayer == null) {
					player.sendMessage("§cEste jogador não está registrado!");
					return false;
				}

				statusPlayer.setLevelStats(status, level);
				statusManager.update(statusPlayer);

				User targetUser = userManager.getUser(target);
				Bukkit.getPluginManager().callEvent(new StatusUpgradeEvent(target, targetUser, status));

				player.sendMessage("§7Você adicionou §a" + level + " leveis §7para o jogador §a" + target.getName() + "§7!");
				return false;
			}
		}
		return false;
	}
}
