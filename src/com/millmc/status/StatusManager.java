package com.millmc.status;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.status.commands.CommandStatus;
import com.millmc.status.storage.StatusSQLStorage;
import com.millmc.status.storage.StatusStorage;
import com.millmcapi.MillAPI;
import com.millmcapi.general.users.UserManager;

public class StatusManager {

	private StatusStorage storage;
	private Map<String, Status> statusPlayers;
	private StatusInventory statusInventory;
	private UserManager userManager;

	public StatusManager() {
		this.storage = new StatusSQLStorage();
		this.statusPlayers = new HashMap<String, Status>();

		MillAPI millAPI = Main.getInstance().getMillAPI();
		this.userManager = millAPI.getModule().getUserManager();

		this.statusInventory = new StatusInventory(this, userManager);

		millAPI.getCommand().registerCommand(new CommandStatus(this, userManager));

		Bukkit.getPluginManager().registerEvents(new StatusListener(this), Main.getInstance());
	}

	public StatusInventory getStatusInventory() {
		return this.statusInventory;
	}

	public void load(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			Status status = storage.download(player.getName());
			if (status == null) {
				status = new Status(player.getName(), 1, 15, 0, 0, 0);
				storage.add(status);
			}

			statusPlayers.put(player.getName(), status);
		});
	}

	public void unload(Player player) {
		update(getStatus(player));
		statusPlayers.remove(player.getName());
	}

	public void update(Status status) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			storage.update(status);
		});
	}

	public Status getStatus(Player player) {
		return getStatus(player.getName());
	}

	public Status getStatus(String name) {
		return statusPlayers.get(name);
	}

}
