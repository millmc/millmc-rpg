package com.millmc.status;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.millmc.Main;
import com.millmc.status.constructors.Stats;
import com.millmc.status.custom.StatusUpgradeEvent;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.custom.UserJoinEvent;
import com.millmcapi.general.users.custom.UserQuitEvent;

public class StatusListener implements Listener {

	private StatusManager statusManager;

	public StatusListener(StatusManager statusManager) {
		this.statusManager = statusManager;
	}

	@EventHandler
	public void onUserJoin(UserJoinEvent event) {
		Player player = event.getPlayer();
		statusManager.load(player);

		tryFixHealth(player);
	}

	public void tryFixHealth(Player player) {
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
			if (player == null || !player.isOnline()) {
				return;
			}
			Status status = statusManager.getStatus(player);
			if (status != null) {
				double maxHealth = status.getMaxHealth(status.getLevelStats(Stats.HEALTH));
				player.setMaxHealth(maxHealth);
			} else {
				tryFixHealth(player);
			}
		}, 10);
	}

	@EventHandler
	public void onUserQuit(UserQuitEvent event) {
		Player player = event.getPlayer();
		statusManager.unload(player);
	}

	@EventHandler
	public void onStatus(StatusUpgradeEvent event) {
		User user = event.getUser();
		Stats stats = event.getStats();

		if (stats == Stats.HEALTH) {
			Player player = user.getPlayer();

			Status status = statusManager.getStatus(player);
			double maxHealth = status.getMaxHealth(status.getLevelStats(stats));
			player.setMaxHealth(maxHealth);
		}
	}
}
