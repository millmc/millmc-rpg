package com.millmc.status;

import com.millmc.status.constructors.Stats;

public class Status {

	private String name;
	private int level_damage;
	private int level_health;
	private int level_damage_crit;
	private int level_chance_crit;
	private int level_chance_block;

	public Status(String name, int level_damage, int level_health, int level_damage_crit, int level_chance_crit, int level_chance_block) {
		this.name = name;
		this.level_damage = level_damage;
		this.level_health = level_health;
		this.level_damage_crit = level_damage_crit;
		this.level_chance_crit = level_chance_crit;
		this.level_chance_block = level_chance_block;
	}

	public String getName() {
		return this.name;
	}

	public int getLevelStats(Stats stats) {
		if (stats == Stats.DAMAGE) {
			return level_damage;
		}
		if (stats == Stats.CRITICAL) {
			return level_damage_crit;
		}
		if (stats == Stats.CRITICAL_CHANCE) {
			return level_chance_crit;
		}
		if (stats == Stats.BLOCK) {
			return level_chance_block;
		}
		if (stats == Stats.HEALTH) {
			return level_health;
		}
		return 0;
	}

	public void addLevelStats(Stats stats) {
		if (stats == Stats.DAMAGE) {
			level_damage += 1;
		} else if (stats == Stats.CRITICAL) {
			level_damage_crit += 1;
		} else if (stats == Stats.CRITICAL_CHANCE) {
			level_chance_crit += 1;
		} else if (stats == Stats.BLOCK) {
			level_chance_block += 1;
		} else if (stats == Stats.HEALTH) {
			level_health += 1;
		}
	}

	public void setLevelStats(Stats stats, int amount) {
		if (stats == Stats.DAMAGE) {
			level_damage = amount;
		} else if (stats == Stats.CRITICAL) {
			level_damage_crit = amount;
		} else if (stats == Stats.CRITICAL_CHANCE) {
			level_chance_crit = amount;
		} else if (stats == Stats.BLOCK) {
			level_chance_block = amount;
		} else if (stats == Stats.HEALTH) {
			level_health = amount;
		}
	}

	public int getChanceBlock(int level) {
		return level;
	}

	public int getChanceCrit(int level) {
		return level;
	}

	public double getDamage(int level) {
		return level;
	}

	public double getDamageCrit(int level) {
		return level / 3.0;
	}

	public double getMaxHealth(int level) {
		return level;
	}
}
