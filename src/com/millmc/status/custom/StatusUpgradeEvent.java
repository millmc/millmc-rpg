package com.millmc.status.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.millmc.status.constructors.Stats;
import com.millmcapi.general.users.User;

public class StatusUpgradeEvent extends PlayerEvent {

	private static final HandlerList handlers = new HandlerList();
	private User user;
	private Stats stats;

	public StatusUpgradeEvent(Player player, User user, Stats stats) {
		super(player);
		this.user = user;
		this.stats = stats;
	}

	public User getUser() {
		return this.user;
	}

	public Stats getStats() {
		return this.stats;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
