package com.millmc.status.storage;

import com.millmc.status.Status;

public interface StatusStorage {

	public Status download(String name);

	public void add(Status status);

	public void update(Status status);

}
