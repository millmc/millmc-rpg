package com.millmc.status.constructors;

public enum Stats {

	DAMAGE("Dano", "4080bbefca87dc0f36536b6508425cfc4b95ba6e8f5e6a46ff9e9cb488a9ed", 25),
	BLOCK("Chance de bloqueio", "6c7d35c7f5c2849d1e23896ebab244d34ef0dafedd8919749461b7d15cc1f04", 25),
	HEALTH("Vida", "e8f477cd308df047f6b92876f85ef874fff044d5127682aebc878db2068", 25),
	CRITICAL("Dano crítico", "e0e18aa78a4b8d4ff7175d4a45d392cc1fb55f9c36d30fc62508892645bc9a9a", 25),
	CRITICAL_CHANCE("Chance crítica", "f13fe2807dc0ea5dd0495d267ec3e55ca375ea621158ca02c42350c435d79771", 25);

	private String name;
	private String skull;
	private int maxlevel;

	private Stats(String name, String skull, int maxlevel) {
		this.name = name;
		this.skull = skull;
		this.maxlevel = maxlevel;
	}

	public String getName() {
		return this.name;
	}

	public String getSkull() {
		return this.skull;
	}

	public int getMaxlevel() {
		return this.maxlevel;
	}
}
