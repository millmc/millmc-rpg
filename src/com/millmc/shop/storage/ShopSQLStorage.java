package com.millmc.shop.storage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;
import com.millmc.Main;
import com.millmc.shop.Shop;
import com.millmcapi.spigot.location.LocationUtils;

public class ShopSQLStorage implements ShopStorage {

	private Connection connection;

	public ShopSQLStorage() {

		try {
			this.connection = Main.getInstance().getMillAPI().getLocalConnection().getConnection();

			Statement stmt = connection.createStatement();
			stmt.execute("CREATE TABLE IF NOT EXISTS `shop` (`Location` text, `shop_main` text);");
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Set<Shop> downloadShops() {
		Set<Shop> shops = new HashSet<Shop>();
		try {
			if (connection != null) {
				PreparedStatement ps = connection.prepareStatement("SELECT * FROM `shop`");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					shops.add(new Gson().fromJson(rs.getString("shop_main"), Shop.class));
				}
				rs.close();
				ps.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return shops;
	}

	@Override
	public void createShop(Shop shop) {
		try {
			if (connection != null) {
				PreparedStatement ps = connection.prepareStatement("INSERT INTO `shop` (location, shop_main) VALUES (?,?)");
				ps.setString(1, LocationUtils.toJson(shop.getLocation()));
				ps.setString(2, new Gson().toJson(shop, Shop.class));
				ps.executeUpdate();
				ps.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteShop(Shop shop) {
		try {

			if (connection != null) {
				PreparedStatement ps = connection.prepareStatement("DELETE FROM `shop` WHERE `location`=?");
				ps.setString(1, LocationUtils.toJson(shop.getLocation()));
				ps.executeUpdate();
				ps.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
