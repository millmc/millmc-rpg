package com.millmc.shop.storage;

import java.util.Set;

import com.millmc.shop.Shop;

public interface ShopStorage {

	public Set<Shop> downloadShops();

	public void createShop(Shop shop);

	public void deleteShop(Shop shop);

}
