package com.millmc.shop.commands;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.millmc.shop.ShopManager;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandShop extends Command {

	private ShopManager shopManager;
	private UserManager userManager;
	private GroupManager groupManager;

	public CommandShop(ShopManager shopManager, UserManager userManager, GroupManager groupManager) {
		super("setshop", "Create shop in server!", "/<command>", Arrays.asList("setloja"));
		this.shopManager = shopManager;
		this.userManager = userManager;
		this.groupManager = groupManager;

	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {

		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player.getName());

		if (!groupManager.check(user.getGroupId(), 80)) {
			player.sendMessage("§cVocê não tem permissão!");
			return false;
		}

		if (args.length == 0) {
			player.sendMessage("§aComandos disponíveis.");
			player.sendMessage("§a/" + this.getName() + " §7<quantia> <venda> <compra>");
			return false;
		} else if (args.length >= 1) {
			if (args.length <= 2) {
				player.sendMessage("§a/" + this.getName() + " §7<quantia> <venda> <compra>");
				return false;
			}

			int amount = 0;
			int priceSell = 0;
			int priceBuy = 0;

			try {

				amount = Integer.parseInt(args[0]);
				priceSell = Integer.parseInt(args[1]);
				priceBuy = Integer.parseInt(args[2]);

			} catch (NumberFormatException e) {
				player.sendMessage("§cUse apenas números!");
				return false;
			}

			if (amount <= 0) {
				player.sendMessage("§cQuantia mínima é 1!");
				return false;
			}

			Block targetBlock = player.getTargetBlock((HashSet<Byte>) null, 4);

			if (targetBlock == null) {
				player.sendMessage("§cVocê precisa estar olhando para uma placa!");
				return false;
			}

			if (targetBlock.getType() != Material.SIGN && targetBlock.getType() != Material.SIGN_POST && targetBlock.getType() != Material.WALL_SIGN) {
				player.sendMessage("§cVocê precisa estar olhando para uma placa!");
				return false;
			}

			ItemStack item = player.getItemInHand();

			if (item == null || item.getType() == Material.AIR) {
				player.sendMessage("§cVocê precisa estar com um item na mão!");
				return false;
			}

			if (priceSell == 0 && priceBuy == 0) {
				player.sendMessage("§cPreço mínimo é 1 para pelo menos um item!");
				return false;
			}

			if (shopManager.getShop(targetBlock.getLocation()) != null) {
				player.sendMessage("§cJá possui uma loja nesta localização!");
				return false;
			}

			Sign sign = (Sign) targetBlock.getState();

			DecimalFormat df = new DecimalFormat("###,###,###,###,###,###");

			sign.setLine(0, "§9§lMillMC");
			sign.setLine(1, String.valueOf(amount));

			if (priceSell != 0 && priceBuy != 0) {
				sign.setLine(2, "§a§lC§0 " + df.format(priceBuy) + ":" + df.format(priceSell) + "§c§l V");
			} else {
				if (priceSell == 0) {
					sign.setLine(2, "§a§lC§0 " + df.format(priceBuy));
				} else if (priceBuy == 0) {
					sign.setLine(2, "§c§lV§0 " + df.format(priceSell));
				} else {
					player.sendMessage("§cNão foi possível criar uma loja aqui!");
					targetBlock.breakNaturally();
					return false;
				}
			}

			String itemName = item.getType().toString().replace("_", " ");
			itemName = WordUtils.capitalize(itemName);

			sign.setLine(3, itemName + (item.getDurability() > 0 ? ":" + item.getDurability() : ""));
			sign.update();

			shopManager.createShop(targetBlock.getLocation(), item, priceSell, priceBuy, amount);
			player.sendMessage("§aLoja criada com sucesso!");
			return false;
		}
		return false;
	}

}
