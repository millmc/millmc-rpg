package com.millmc.shop;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import com.millmc.Main;
import com.millmc.shop.commands.CommandShop;
import com.millmc.shop.storage.ShopSQLStorage;
import com.millmcapi.MillAPI;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.item.Item;

public class ShopManager {

	private ShopSQLStorage storage;
	private Set<Shop> shops;

	public ShopManager() {
		this.storage = new ShopSQLStorage();
		this.shops = storage.downloadShops();

		MillAPI millAPI = Main.getInstance().getMillAPI();

		Module module = millAPI.getModule();
		UserManager userManager = module.getUserManager();
		GroupManager groupManager = module.getGroupManager();

		millAPI.getCommand().registerCommand(new CommandShop(this, userManager, groupManager));

		Bukkit.getPluginManager().registerEvents(new ShopListener(this, userManager, groupManager), Main.getInstance());
	}

	public Set<Shop> getShops() {
		return this.shops;
	}

	public void createShop(Location location, ItemStack item, int sellPrice, int buyPrice, int amount) {
		Shop shop = new Shop(location, new Item(item).toJson(), sellPrice, buyPrice, amount);
		shops.add(shop);
		storage.createShop(shop);
	}

	public void deleteShop(Location location) {
		Shop shop = getShop(location);
		if (shop != null) {
			shops.remove(shop);
			storage.deleteShop(shop);
		}
	}

	public Shop getShop(Location location) {
		return shops.stream().filter(i -> i.getLocation().equals(location)).findAny().orElse(null);
	}

}
