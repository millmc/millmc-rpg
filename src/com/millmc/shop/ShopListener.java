package com.millmc.shop;

import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.millmc.Main;
import com.millmc.economy.EconomyManager;
import com.millmc.economy.custom.PlayerMoneyChangeEvent;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class ShopListener implements Listener {

	private DecimalFormat decimalFormat;
	private EconomyManager economyManager;
	private ShopManager shopManager;
	private UserManager userManager;
	private GroupManager groupManager;

	public ShopListener(ShopManager shopManager, UserManager userManager, GroupManager groupManager) {
		this.shopManager = shopManager;
		this.userManager = userManager;
		this.groupManager = groupManager;
		this.decimalFormat = new DecimalFormat("###,###,###,###,###,###.00");

		this.economyManager = Main.getInstance().getEconomyManager();
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBreak(BlockBreakEvent event) {
		if (event.isCancelled()) {
			return;
		}

		Player player = event.getPlayer();
		Block block = event.getBlock();

		if (block.getType() == Material.SIGN || block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {

			if (shopManager.getShop(block.getLocation()) == null) {
				return;
			}

			Shop shop = shopManager.getShop(block.getLocation());

			User user = userManager.getUser(player.getName());
			if (!groupManager.check(user.getGroupId(), 80)) {
				player.sendMessage("§cVocê não tem permissão!");
				event.setCancelled(true);
				return;
			}

			if (player.getGameMode() != GameMode.CREATIVE) {
				player.sendMessage("§cVocê precisa estar no criativo!");
				event.setCancelled(true);
				return;
			}

			shopManager.deleteShop(shop.getLocation());
			player.sendMessage("§cLoja deletada com sucesso!");
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();

		if (event.getAction().toString().toLowerCase().contains("block")) {
			Block block = event.getClickedBlock();

			if (block == null) {
				return;
			}

			if (block.getType() == Material.AIR) {
				return;
			}

			if (block.getType() == Material.SIGN || block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {
				Sign sign = (Sign) block.getState();

				String line1 = sign.getLine(0);

				if (!line1.equalsIgnoreCase("§9§lMillMC")) {
					return;
				}

				if (shopManager.getShop(block.getLocation()) == null) {
					return;
				}

				Shop shop = shopManager.getShop(block.getLocation());

				int buyPrice = shop.getBuyPrice();
				int sellPrice = shop.getSellPrice();

				if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (buyPrice == 0) {
						player.sendMessage("§cVocê não pode comprar aqui!");
						return;
					}
					int price = buyPrice;

					if (player.getInventory().firstEmpty() < 0) {
						player.sendMessage("§cVocê está com o inventário cheio!");
						return;
					}

					if (economyManager.getBalance(player.getName()) < price) {
						player.sendMessage("§cVocê não tem coins suficientes!");
						return;
					}

					ItemStack item = shop.getItem().clone();
					item.setAmount(shop.getAmount());

					economyManager.withdrawPlayer(player.getName(), price);
					Bukkit.getPluginManager().callEvent(new PlayerMoneyChangeEvent(player, economyManager.getBalance(player.getName())));

					player.getInventory().addItem(item);

					player
							.sendMessage("§aVocê comprou " + shop.getAmount() + " " + shop.getItem().getType().toString() + ", pelo preço de " + decimalFormat.format(price));
					return;
				}

				if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
					if (sellPrice == 0) {
						player.sendMessage("§cVocê não pode vender aqui!");
						return;
					}

					ItemStack item = shop.getItem().clone();
					ItemStack[] playerItems = player.getInventory().getContents();

					int amount_sell = 0;

					for (ItemStack i : playerItems) {
						if (i != null) {
							if (i.getType() == item.getType() && i.getData().equals(item.getData())) {
								amount_sell += i.getAmount();
								player.getInventory().remove(i);
							}
						}
					}

					if (amount_sell == 0) {
						player.sendMessage("§cVocê não tem itens para vender!");
						return;
					}

					double price_per_unit = (double) sellPrice / (double) shop.getAmount();
					long price = (long) (price_per_unit * amount_sell);

					player.getInventory().removeItem(item);
					economyManager.depositPlayer(player.getName(), price);
					Bukkit.getPluginManager().callEvent(new PlayerMoneyChangeEvent(player, economyManager.getBalance(player.getName())));

					player.sendMessage("§aVocê vendeu " + amount_sell + " " + shop.getItem().getType().toString() + ", pelo preço de " + decimalFormat.format(price));
					return;
				}

			}
		}
	}
}
