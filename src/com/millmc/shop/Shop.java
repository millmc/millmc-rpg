package com.millmc.shop;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import com.millmcapi.spigot.item.Item;

public class Shop {

	private String world;
	private int x;
	private int y;
	private int z;

	private String item;
	private int sellPrice;
	private int buyPrice;
	private int amount;

	public Shop(Location location, String item, int sellPrice, int buyPrice, int amount) {

		this.world = location.getWorld().getName();
		this.x = (int) location.getX();
		this.y = (int) location.getY();
		this.z = (int) location.getZ();

		this.item = item;
		this.sellPrice = sellPrice;
		this.buyPrice = buyPrice;
		this.amount = amount;
	}

	public String getWorld() {
		return this.world;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getZ() {
		return this.z;
	}

	public int getSellPrice() {
		return this.sellPrice;
	}

	public int getBuyPrice() {
		return this.buyPrice;
	}

	public ItemStack getItem() {
		return new Item().fromJson(item);
	}

	public int getAmount() {
		return this.amount;
	}

	public Location getLocation() {
		return new Location(Bukkit.getWorld(world), x, y, z);
	}

}
