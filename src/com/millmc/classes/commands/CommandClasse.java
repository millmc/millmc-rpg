package com.millmc.classes.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.classes.ClassManager;
import com.millmc.classes.Classe;
import com.millmcapi.general.groups.GroupManager;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class CommandClasse extends Command {

	private ClassManager classManager;
	private UserManager userManager;
	private GroupManager groupManager;

	public CommandClasse(ClassManager classManager) {
		super("class", "", "", Arrays.asList("classes", "classe"));
		Module module = Main.getInstance().getMillAPI().getModule();

		this.classManager = classManager;
		this.userManager = module.getUserManager();
		this.groupManager = module.getGroupManager();
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player);

		if (args.length == 0) {
			classManager.getClassInventory().openSelector(player);
		} else if (args.length >= 1) {
			if (!groupManager.check(user.getGroupId(), 90)) {
				player.sendMessage("§cVocê não tem permissão para usar esse comando!");
				return false;
			}

			if (args[0].equalsIgnoreCase("set")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando: §a/" + arg1 + " set <jogador> <classe>");
					return false;
				}

				Player target = Bukkit.getPlayer(args[1]);

				if (target == null || !target.getName().equalsIgnoreCase(args[1])) {
					player.sendMessage("§cEste jogador não está conectado!");
					return false;
				}

				Classe classe = classManager.getClasse(args[2]);

				if (classe == null) {
					player.sendMessage("§cNão foi encontrado uma classe com o nome " + args[2]);
					return false;
				}

				User userTarget = userManager.getUser(target);
				userTarget.setClassName(classe.getName());
				userManager.update(userTarget);

				player.sendMessage("§7Você alterou o classe de §a" + target.getName() + "§7 para §a" + classe.getName() + "§7!");
				return false;
			}
		}
		return false;
	}
}
