package com.millmc.classes.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.millmc.Main;
import com.millmc.classes.Classe;
import com.millmc.damage.custom.UserDamageByUserEvent;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.reflection.Particles;
import com.millmcapi.spigot.utils.TimeUtils;

import net.minecraft.server.v1_8_R3.EnumParticle;

public class Vampire implements Classe {

	private static final int MAX_BATS = 6;
	private Map<Integer, Long> cooldown;
	private UserManager userManager;

	public Vampire(UserManager userManager) {
		this.cooldown = new HashMap<Integer, Long>();
		this.userManager = userManager;
		Bukkit.getPluginManager().registerEvents(getListener(), Main.getInstance());
	}

	@Override
	public String getName() {
		return "Vampiro";
	}

	@Override
	public String getIcon() {
		return "✹";
	}

	@Override
	public String getPrefix() {
		return "§7Vampiro";
	}

	@Override
	public Listener getListener() {
		return new Listener() {

			@EventHandler
			public void onInteract(PlayerInteractEvent event) {
				Player player = event.getPlayer();

				if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (player.isSneaking()) {
						User user = userManager.getUser(player);
						if (user.getClassName().equalsIgnoreCase(getName())) {
							if (cooldown.containsKey(user.getId()) && cooldown.get(user.getId()) >= System.currentTimeMillis()) {
								int time = (int) (cooldown.get(user.getId()) - System.currentTimeMillis()) / 1000;
								player.sendMessage("§7Você precisa aguardar mais §a" + new TimeUtils(time).toTime() + "§7!");
								return;
							}
							// cooldown.put(user.getId(), System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5));
							cooldown.put(user.getId(), System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(5));

							Location location = player.getLocation().clone().add(0, 1, 0);

							Map<Entity, Long> entities = new HashMap<Entity, Long>();

							for (int x = 0; x <= MAX_BATS; x++) {
								Entity bat = location.getWorld().spawnEntity(location, EntityType.BAT);
								entities.put(bat, System.currentTimeMillis() + 2000);
							}

							new BukkitRunnable() {

								@Override
								public void run() {
									if (entities.size() <= 0) {
										cancel();
									}

									Map<Entity, Long> entities2 = new HashMap<Entity, Long>(entities);
									for (Entry<Entity, Long> i : entities2.entrySet()) {
										Entity entityKey = i.getKey();
										Long timeToRemove = i.getValue();

										Particles.sendParticle(EnumParticle.FIREWORKS_SPARK, entityKey.getLocation(), 0.0003f, 0.0003f, 0.0003f, 0.0003f, 1);

										if (System.currentTimeMillis() >= timeToRemove) {
											entityKey.remove();
											entities.remove(entityKey);
											continue;
										}

										entityKey.getLocation().getWorld().getNearbyEntities(entityKey.getLocation(), 1.5, 1.5, 1.5).forEach(entity -> {
											if (!entity.isDead()) {
												if (!(entity instanceof Bat)) {
													if (entity instanceof Player && ((Player) entity).equals(player)) {
													} else {
														if (entity instanceof Player) {
															msg((Player) entity, "§cVocê tomou dano de um morcego de um vampiro!");
														}
														if (entity instanceof Damageable) {
															((Damageable) entity).damage(2.0, player);
														}
													}
												}
											}
										});

									}
								}
							}.runTaskTimerAsynchronously(Main.getInstance(), 0, 5);

							player.sendMessage("§7Você usou sua habilidade especial!");
						}
					}
				}
			}

			@EventHandler
			public void onDamage(UserDamageByUserEvent event) {
				User damager = event.getDamager();
				Player player = damager.getPlayer();

				if (damager.getClassName().equalsIgnoreCase(getName())) {
					double maxHealth = player.getMaxHealth();

					// 2.0 - 1 coração
					// 1.0 - 1/2 coração
					double heal = player.getHealth() + 0.33333333333;
					player.setHealth(heal > maxHealth ? maxHealth : heal);
				}
			}
		};
	}

	private Map<String, Long> m = new HashMap<String, Long>();

	private void msg(Player player, String msg) {
		if (m.containsKey(player.getName()) && m.get(player.getName()) >= System.currentTimeMillis()) {
			return;
		}
		m.put(player.getName(), System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(2));
		player.sendMessage(msg);
	}

}
