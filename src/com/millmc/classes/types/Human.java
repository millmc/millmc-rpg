package com.millmc.classes.types;

import org.bukkit.event.Listener;

import com.millmc.classes.Classe;

public class Human implements Classe {

	@Override
	public String getName() {
		return "Humano";
	}

	@Override
	public String getIcon() {
		return "❂";
	}

	@Override
	public String getPrefix() {
		return "§7Humano";
	}

	@Override
	public Listener getListener() {
		return new Listener() {
		};
	}
}
