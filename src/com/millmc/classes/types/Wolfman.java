package com.millmc.classes.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWolf;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.millmc.Main;
import com.millmc.classes.Classe;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.event.update.UpdateEvent;
import com.millmcapi.spigot.event.update.UpdateType;
import com.millmcapi.spigot.utils.TimeUtils;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntityWolf;
import net.minecraft.server.v1_8_R3.PathfinderGoalFollowOwner;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;

public class Wolfman implements Classe {

	private static final int MAX_WOLFS = 4;
	private UserManager userManager;
	private Map<Integer, Long> cooldown;

	public Wolfman(UserManager userManager) {
		this.userManager = userManager;
		this.cooldown = new HashMap<Integer, Long>();
		Bukkit.getPluginManager().registerEvents(getListener(), Main.getInstance());
	}

	@Override
	public String getName() {
		return "Lobisomem";
	}

	@Override
	public String getIcon() {
		return "✵";
	}

	@Override
	public String getPrefix() {
		return "§7Lobisomem";
	}

	@Override
	public Listener getListener() {
		return new Listener() {

			@EventHandler
			public void onInteract(PlayerInteractEvent event) {
				Player player = event.getPlayer();

				if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (player.isSneaking()) {
						User user = userManager.getUser(player);
						if (user.getClassName().equalsIgnoreCase(getName())) {
							if (cooldown.containsKey(user.getId()) && cooldown.get(user.getId()) >= System.currentTimeMillis()) {
								int time = (int) (cooldown.get(user.getId()) - System.currentTimeMillis()) / 1000;
								player.sendMessage("§7Você precisa aguardar mais §a" + new TimeUtils(time).toTime() + "§7!");
								return;
							}
							// cooldown.put(user.getId(), System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5));
							cooldown.put(user.getId(), System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(17));

							Location location = player.getLocation().clone().add(0, 1, 0);

							Map<Entity, Long> entities = new HashMap<Entity, Long>();

							for (int x = 0; x <= MAX_WOLFS; x++) {
								Wolf wolf = (Wolf) location.getWorld().spawnEntity(location, EntityType.WOLF);
								entities.put(wolf, System.currentTimeMillis() + 15000);
							}

							new BukkitRunnable() {

								@SuppressWarnings("unchecked")
								@Override
								public void run() {
									if (entities.size() <= 0) {
										cancel();
									}

									Map<Entity, Long> entities2 = new HashMap<Entity, Long>(entities);
									for (Entry<Entity, Long> i : entities2.entrySet()) {
										Wolf wolf = (Wolf) i.getKey();
										Long timeToRemove = i.getValue();

										if (System.currentTimeMillis() >= timeToRemove) {
											wolf.remove();
											entities.remove(i.getKey());
											continue;
										}

										EntityWolf handle = ((CraftWolf) wolf).getHandle();
										if (wolf.getLocation().distance(player.getLocation()) > 10) {
											handle.goalSelector.a(new PathfinderGoalFollowOwner(handle, 2.0, 2.0f, 2.0f));
										} else {
											Location l = wolf.getLocation();

											Entity nearby = l.getWorld().getNearbyEntities(l, 3, 3, 3).stream().filter(i2 -> i2 instanceof Player && !((Player) i2).equals(player))
													.findAny().orElse(null);
											if (nearby != null) {
												handle.setGoalTarget(((EntityLiving) ((CraftPlayer) player).getHandle()));
												handle.goalSelector.a(new PathfinderGoalMeleeAttack(((EntityCreature) handle),
														(Class<? extends net.minecraft.server.v1_8_R3.Entity>) nearby.getClass(), 2.0, true));
											}
										}
									}
								}
							}.runTaskTimerAsynchronously(Main.getInstance(), 0, 5);

							player.sendMessage("§7Você usou sua habilidade especial!");
						}
					}
				}
			}

			@EventHandler
			public void onTime(UpdateEvent event) {
				if (event.getUpdateType() == UpdateType.SEC_4) {
					Set<User> users = userManager.getDownloadedUsers().stream().filter(i -> i.getClassName().equalsIgnoreCase(getName())).collect(Collectors.toSet());

					if (users.size() > 0) {
						users.forEach(i -> {
							Player player = i.getPlayer();

							if (player != null) {

								long playerTime = player.getWorld().getTime();

								if (playerTime >= 0 && playerTime <= 12000) {
									// sun
								} else {
									player.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
									player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 5 * 20, 0));

									player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
									player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 5 * 20, 0));
								}
							}
						});
					}
				}
			}
		};
	}

}
