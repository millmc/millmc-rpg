package com.millmc.classes;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.millmc.Main;
import com.millmc.classes.commands.CommandClasse;
import com.millmc.classes.types.Human;
import com.millmc.classes.types.Vampire;
import com.millmc.classes.types.Wolfman;
import com.millmcapi.MillAPI;
import com.millmcapi.general.module.Module;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.npc.NPC;
import com.millmcapi.spigot.npc.type.NPCType;

public class ClassManager {

	private Map<String, Classe> classes;
	private ClassInventory classInventory;

	public ClassManager() {
		this.classes = new HashMap<String, Classe>();

		MillAPI millAPI = Main.getInstance().getMillAPI();
		NPC npc = new NPC(NPCType.JOKER, "selectorClasses", new Location(Bukkit.getWorld("world"), -531, 7, 694), "§e§lCLASSES");
		npc.getNPCHologram().getHologram().addLine("§7§o(Clique para selecionar)");

		Module module = millAPI.getModule();
		module.getNpcManager().registerNPC(npc, false);

		millAPI.getCommand().registerCommand(new CommandClasse(this));

		UserManager userManager = module.getUserManager();

		Human human = new Human();
		classes.put(human.getName(), human);

		Vampire vampire = new Vampire(userManager);
		classes.put(vampire.getName(), vampire);

		Wolfman lobisomen = new Wolfman(userManager);
		classes.put(lobisomen.getName(), lobisomen);

		this.classInventory = new ClassInventory(this, userManager);
		Bukkit.getPluginManager().registerEvents(classInventory, Main.getInstance());
	}

	public ClassInventory getClassInventory() {
		return this.classInventory;
	}

	public Map<String, Classe> getClasses() {
		return this.classes;
	}

	public Classe getClasse(String name) {
		return classes.get(name);
	}
}
