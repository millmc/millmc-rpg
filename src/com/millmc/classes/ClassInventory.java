package com.millmc.classes;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;
import com.millmcapi.spigot.item.Item;
import com.millmcapi.spigot.npc.npcevent.NPCInteractEvent;

public class ClassInventory implements Listener {

	private UserManager userManager;
	private ClassManager classManager;

	public ClassInventory(ClassManager classManager, UserManager userManager) {
		this.classManager = classManager;
		this.userManager = userManager;
	}

	public void openSelector(Player player) {
		Inventory inv = Bukkit.createInventory(null, 3 * 9, "§7Classes");

		inv.setItem(11, new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setDisplayName("§7✹ Vampiro")
				.setCustomSkull2("8d44756e0b4ece8d746296a3d5e297e1415f4ba17647ffe228385383d161a9")
				.setLore(
						"",
						"§a • §7Vantagem: §aao hitar um jogador",
						"§a   irá regenerar sua vida aos poucos.",
						"",
						"§a • §7Desvantagem: §ao dano é maior ao",
						"§a   ser hitado por espadas com essência",
						"§a   de fogo.",
						""
						)
				.addString("class", "Vampiro"));

		inv.setItem(13, new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setDisplayName("§7❂ Humano")
				.setCustomSkull2("5671877b4a14913e789e57cb984a3ed8b122901fffdbeeca42757b87fe8f467")
				.setLore(
						"",
						"§a • §7Vantagem: §ano seu 5º ataque",
						"§a   irá acrescentar mais dano.",
						"",
						"§a • §7Desvantagem: §anão há.",
						"")
				.addString("class", "Humano"));

		inv.setItem(15, new Item(Material.SKULL_ITEM).setDurabilitys((short) 3).setDisplayName("§7✵ Lobisomem")
				.setCustomSkull2("20c7f885326ba0949c316696d19d53082b994e9b48cad6673558dd6c5bcab49")
				.setLore(
						"",
						"§a • §7Vantagem: §aao anoitecer receberá",
						"§a   efeitos de resistência e força.",
						"",
						"§a • §7Desvantagem: §ao dano é maior ao",
						"§a   ser hitado por espadas com essência",
						"§a   de prata.",
						"")
				.addString("class", "Lobisomem"));

		player.openInventory(inv);
	}

	@EventHandler
	public void onNpc(NPCInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getNpc().identifier().equalsIgnoreCase("selectorClasses")) {
			User user = userManager.getUser(player.getName());
			if (user.getClassName().equalsIgnoreCase("nothing")) {
				openSelector(player);
			} else {
				player.sendMessage("§cApenas pessoas que não possuem classe podem interagir aqui!");
			}
		}
	}

	@EventHandler
	public void onInv(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();

		if (player.getOpenInventory() == null) {
			return;
		}

		if (!player.getOpenInventory().getTitle().equalsIgnoreCase("§7Classes")) {
			return;
		}
		event.setCancelled(true);

		ItemStack itemStack = event.getCurrentItem();

		if (itemStack == null || itemStack.getType() == Material.AIR) {
			return;
		}

		Item item = new Item(itemStack);

		if (!item.hasKey("class")) {
			return;
		}

		User user = userManager.getUser(player.getName());

		if (!user.getClassName().equalsIgnoreCase("nothing")) {
			player.sendMessage("§cVocê já possui uma classe!");
			return;
		}

		Classe classe = classManager.getClasse(item.getString("class"));

		if (classe == null) {
			player.sendMessage("§c§lERR0R: §cocorreu um erro ao procurar esta classe, informe um membro da equipe!");
			return;
		}

		user.setClassName(classe.getName());
		userManager.update(user);
		player.sendMessage("§7Você selecionou a classe §a" + classe.getName() + "§7!");
		player.closeInventory();

		player.teleport(new Location(Bukkit.getWorld("world"), -547, 8, 694));
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();

		User user = userManager.getUser(player.getName());
		if (user.getClassName().equalsIgnoreCase("nothing")) {
			event.setCancelled(true);
			player.sendMessage("§7É necessário escolher sua classe para poder digitar comandos!");
		}
	}
}
