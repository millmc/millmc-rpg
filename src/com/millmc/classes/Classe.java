package com.millmc.classes;

import org.bukkit.event.Listener;

public interface Classe {

	public String getName();

	public String getIcon();

	public String getPrefix();

	public Listener getListener();

}
