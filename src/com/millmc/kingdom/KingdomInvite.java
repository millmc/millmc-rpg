package com.millmc.kingdom;

import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class KingdomInvite {

	private String sender;
	private String target;
	private long expire;

	public KingdomInvite(Player sender, Player target) {
		this.sender = sender.getName();
		this.target = target.getName();
		this.expire = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(1);
	}

	public String getSenderName() {
		return this.sender;
	}

	public Player getSender() {
		return Bukkit.getPlayer(this.sender);
	}

	public String getTargetName() {
		return this.target;
	}

	public Player getTarget() {
		return Bukkit.getPlayer(this.target);
	}

	public long getExpire() {
		return this.expire;
	}

	public boolean isExpired() {
		return System.currentTimeMillis() >= expire;
	}

}
