package com.millmc.kingdom;

import java.util.List;

public class Kingdom {

	private int leader;
	private String name;
	private String description;
	private List<Integer> members;

	public Kingdom(int leader, String name, String description, List<Integer> members) {
		this.leader = leader;
		this.name = name;
		this.description = description;
		this.members = members;

		members.add(leader);
	}

	public int getLeader() {
		return this.leader;
	}

	public void setLeader(int leader) {
		this.leader = leader;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Integer> getMembers() {
		return this.members;
	}

	public void addMember(int id) {
		members.add(id);
	}

	public void removeMember(int id) {
		members.remove(id);
	}

}
