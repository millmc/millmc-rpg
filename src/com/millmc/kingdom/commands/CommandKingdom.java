package com.millmc.kingdom.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.millmc.kingdom.Kingdom;
import com.millmc.kingdom.KingdomInvite;
import com.millmc.kingdom.KingdomManager;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandKingdom extends Command {

	private KingdomManager kingdomManager;
	private UserManager userManager;

	public CommandKingdom(KingdomManager kingdomManager, UserManager userManager) {
		super("reino", "", "", Arrays.asList("reinos", "kingdom"));
		this.kingdomManager = kingdomManager;
		this.userManager = userManager;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean execute(CommandSender sender, String commandName, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		User user = userManager.getUser(player);

		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("criar")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando §a/" + commandName + " criar <nome>");
					return false;
				}

				Kingdom kingdomNow = kingdomManager.getKingdomById(user.getId());
				if (kingdomNow != null) {
					player.sendMessage("§7Você já pertence a um reino!");
					return false;
				}

				String name = args[1];

				if (hasIlegalCharacters(name)) {
					player.sendMessage("§7Seu reino pode apenas ter letras e números!");
					return false;
				}

				if (name.length() > 8) {
					player.sendMessage("§7Seu reino não pode ter um nome superior a 8 letras!");
					return false;
				}

				if (kingdomManager.getKingdomsName().contains(name.toLowerCase())) {
					player.sendMessage("§7Já possui um reino cadastrado com o nome §a" + name + "§7.");
					return false;
				}

				Kingdom kingdom = new Kingdom(user.getId(), name, "", new ArrayList<Integer>());
				kingdomManager.add(kingdom);
				player.sendMessage("§7Você acabou de fundar seu reino!");
				return false;
			}

			if (args[0].equalsIgnoreCase("deletar")) {
				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom == null) {
					player.sendMessage("§7Você não pertence a nenhum reino!");
					return false;
				}

				if (kingdom.getLeader() != user.getId()) {
					player.sendMessage("§7Você não é o lider deste reino!");
					return false;
				}

				kingdomManager.sendMessage(kingdom, "§a" + player.getName() + "§7 deletou do reino!");
				kingdomManager.remove(kingdom);
				player.sendMessage("§7Seu reino foi deletado com sucesso!");
				return false;
			}

			if (args[0].equalsIgnoreCase("sair")) {
				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom == null) {
					player.sendMessage("§7Você não pertence a nenhum reino!");
					return false;
				}

				if (kingdom.getLeader() == user.getId()) {
					player.sendMessage("§7Você é o lider deste reino!");
					return false;
				}

				kingdomManager.sendMessage(kingdom, "§a" + player.getName() + "§7 saiu do reino!");

				kingdom.removeMember(user.getId());
				kingdomManager.getUserTag().remove(user.getId());
				kingdomManager.update(kingdom);
				player.sendMessage("§7Você saiu do reino!");
				return false;
			}

			if (args[0].equalsIgnoreCase("transferir")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando §a/" + commandName + " transferir <jogador>");
					return false;
				}

				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom == null) {
					player.sendMessage("§7Você não pertence a nenhum reino!");
					return false;
				}

				if (kingdom.getLeader() != user.getId()) {
					player.sendMessage("§7Você não é o lider deste reino!");
					return false;
				}

				Player target = Bukkit.getPlayer(args[1]);
				if (target == null || !target.getName().equalsIgnoreCase(args[1])) {
					player.sendMessage("§7O jogador precisa estar online!");
					return false;
				}

				if (target.equals(player)) {
					player.sendMessage("§7Você não pode transferir a liderança para você mesmo!");
					return false;
				}

				User userTarget = userManager.getUser(target);

				Kingdom kingdomTarget = kingdomManager.getKingdomById(userTarget.getId());

				if (kingdomTarget == null) {
					player.sendMessage("§7O jogador precisa estar em seu reino para transferir a liderança!");
					return false;
				}

				if (!kingdomTarget.equals(kingdom)) {
					player.sendMessage("§7Este jogador não está no seu reino!");
					return false;
				}

				kingdom.setLeader(userTarget.getId());
				kingdomManager.update(kingdom);

				kingdomManager.sendMessage(kingdom, "§a" + player.getName() + "§7 transferiu a liderança para §a" + target.getName() + "§7!");

				player.sendMessage("§7Você transferiu a liderança do seu reino para §a" + target.getName() + "§7!");
				return false;
			}

			if (args[0].equalsIgnoreCase("expulsar")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando §a/" + commandName + " expulsar <jogador>");
					return false;
				}

				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom == null) {
					player.sendMessage("§7Você não pertence a nenhum reino!");
					return false;
				}

				if (kingdom.getLeader() != user.getId()) {
					player.sendMessage("§7Você não é o lider deste reino!");
					return false;
				}

				String name = Bukkit.getOfflinePlayer(args[1]).getName();
				Kingdom kingdomTarget = kingdomManager.getKingdomByName(name);

				if (player.getName().equalsIgnoreCase(name)) {
					player.sendMessage("§7Você não pode fazer isso com você mesmo!");
					return false;
				}

				if (kingdomTarget == null) {
					player.sendMessage("§7Este jogador não está no seu reino!");
					return false;
				}

				if (!kingdomTarget.equals(kingdom)) {
					player.sendMessage("§7Este jogador não está no seu reino!");
					return false;
				}

				int targetId = 0;
				User targetUser = userManager.getUser(name);
				if (targetUser != null) {
					targetId = targetUser.getId();
				} else {
					targetUser = userManager.getUserTemporality(name);

					if (targetUser == null) {
						player.sendMessage("§7Usuário não cadastrado!");
						return false;
					}

					targetId = targetUser.getId();
				}

				kingdomManager.sendMessage(kingdom, "§a" + args[1] + "§7 foi expulso do reino!");

				kingdom.removeMember(targetId);
				kingdomManager.getUserTag().remove(targetId);
				kingdomManager.update(kingdom);
				player.sendMessage("§a" + args[1] + " §7foi expulso com sucesso!");
				return false;
			}

			if (args[0].equalsIgnoreCase("convidar")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando §a/" + commandName + " convidar <jogador>");
					return false;
				}

				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom == null) {
					player.sendMessage("§7Você não pertence a nenhum reino!");
					return false;
				}

				if (kingdom.getLeader() != user.getId()) {
					player.sendMessage("§7Você não é o lider deste reino!");
					return false;
				}

				Player target = Bukkit.getPlayer(args[1]);

				if (target == null || !target.getName().equalsIgnoreCase(args[1])) {
					player.sendMessage("§7Este jogador não está conectado!");
					return false;
				}

				if (player.getName().equalsIgnoreCase(target.getName())) {
					player.sendMessage("§7Você não pode fazer isso com você mesmo!");
					return false;
				}

				Kingdom kingdomTarget = kingdomManager.getKingdomByName(target.getName());

				if (kingdomTarget != null) {
					player.sendMessage("§7Este jogador já está em um reino!");
					return false;
				}

				Set<KingdomInvite> invitesReceived = kingdomManager.getInvitesReceived(target);
				if (invitesReceived.stream().filter(i -> i.getSender().equals(player)).findAny().orElse(null) != null) {
					player.sendMessage("§7Você já enviou um convite para esse jogador!");
					return false;
				}

				KingdomInvite kingdomInvite = new KingdomInvite(player, target);
				kingdomManager.getInvites().add(kingdomInvite);

				player.sendMessage("§7Você convidou §a" + target.getName() + "§7 para juntar-se ao seu reino!");

				target.sendMessage("§7Você foi convidado para juntar-se ao reino §a" + kingdom.getName());
				TextComponent tx1 = new TextComponent("§7Clique para ");
				TextComponent tx11 = new TextComponent("§7 ou ");

				TextComponent tx2 = new TextComponent("§a§lACEITAR");
				tx2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reino aceitar " + player.getName()));
				tx2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7/reino aceitar " + player.getName()).create()));

				TextComponent tx3 = new TextComponent("§c§lRECUSAR");
				tx3.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reino recusar " + player.getName()));
				tx3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7/reino recusar " + player.getName()).create()));

				target.spigot().sendMessage(tx1, tx2, tx11, tx3);
				return false;
			}

			if (args[0].equalsIgnoreCase("aceitar")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando §a/" + commandName + " aceitar <jogador>");
					return false;
				}

				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom != null) {
					player.sendMessage("§7Você já pertence a um reino!");
					return false;
				}

				Set<KingdomInvite> invitesReceived = kingdomManager.getInvitesReceived(player);

				if (invitesReceived.size() == 0) {
					player.sendMessage("§7Você não foi convidado para participar de um reino!");
					return false;
				}

				KingdomInvite invite = invitesReceived.stream().filter(i -> i.getSenderName().equalsIgnoreCase(args[1])).findAny().orElse(null);
				if (invite == null) {
					player.sendMessage("§7Você não recebeu convites para entrar no reino de §a" + args[1]);
					return false;
				}
				String name = Bukkit.getOfflinePlayer(args[1]).getName();
				Kingdom kingdomTarget = kingdomManager.getKingdomByName(name);

				kingdomManager.getInvites().remove(invite);

				if (kingdomTarget == null) {
					player.sendMessage("§7O convite deste jogador tornou-se inválido!");
					return false;
				}

				kingdomTarget.addMember(user.getId());
				kingdomManager.getUserTag().put(user.getId(), kingdomTarget.getName());
				kingdomManager.update(kingdomTarget);

				kingdomManager.sendMessage(kingdomTarget, "§a" + player.getName() + "§7 entrou no reino!");
				player.sendMessage("§7Convite aceito com sucesso!");
				return false;
			}

			if (args[0].equalsIgnoreCase("recusar")) {
				if (args.length == 1) {
					player.sendMessage("§7Use o comando §a/" + commandName + " recusar <jogador>");
					return false;
				}

				Kingdom kingdom = kingdomManager.getKingdomById(user.getId());
				if (kingdom != null) {
					player.sendMessage("§7Você já pertence a um reino!");
					return false;
				}

				Set<KingdomInvite> invitesReceived = kingdomManager.getInvitesReceived(player);

				if (invitesReceived.size() == 0) {
					player.sendMessage("§7Você não foi convidado para participar de um reino!");
					return false;
				}

				KingdomInvite invite = invitesReceived.stream().filter(i -> i.getSenderName().equalsIgnoreCase(args[1])).findAny().orElse(null);
				if (invite == null) {
					player.sendMessage("§7Você não recebeu convites para entrar no reino de §a" + args[1]);
					return false;
				}
				String name = Bukkit.getOfflinePlayer(args[1]).getName();
				Kingdom kingdomTarget = kingdomManager.getKingdomByName(name);

				kingdomManager.getInvites().remove(invite);

				if (kingdomTarget == null) {
					player.sendMessage("§7O convite deste jogador tornou-se inválido!");
					return false;
				}

				player.sendMessage("§7Convite recusado com sucesso!");
				invite.getSender().sendMessage("§a" + player.getName() + "§7 recusou o convite para juntar-se ao reino!");
				return false;
			}
		}
		return false;
	}

	public boolean hasIlegalCharacters(String string) {
		Pattern pattern = Pattern.compile("[~#@*+%{}<>\\[\\]|\"\\_^]");
		Matcher matcher = pattern.matcher(string);
		return matcher.find();
	}

}
