package com.millmc.kingdom.storage;

import java.util.Set;

import com.millmc.kingdom.Kingdom;

public interface KingdomStorage {

	public Set<Kingdom> download();

	public void add(Kingdom kingdom);

	public void remove(Kingdom kingdom);

	public void update(Kingdom kingdom);

}
