package com.millmc.kingdom;

import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.millmcapi.spigot.event.update.async.AsyncUpdateEvent;
import com.millmcapi.spigot.event.update.async.AsyncUpdateType;

public class KingdomListener implements Listener {

	private KingdomManager kingdomManager;

	public KingdomListener(KingdomManager kingdomManager) {
		this.kingdomManager = kingdomManager;
	}

	@EventHandler
	public void onTime(AsyncUpdateEvent event) {
		if (event.getUpdateType() == AsyncUpdateType.SEC) {
			Set<KingdomInvite> invites = kingdomManager.getInvites().stream().filter(i -> i.isExpired()).collect(Collectors.toSet());

			invites.forEach(i -> {
				Player sender = i.getSender();
				if (sender != null) {
					sender.sendMessage(KingdomInviteExpireType.TIME_OUT_SENDER.getMessage(i.getTargetName()));
				}

				Player target = i.getTarget();
				if (target != null) {
					target.sendMessage(KingdomInviteExpireType.TIME_OUT_TARGET.getMessage(i.getSenderName()));
				}

				kingdomManager.getInvites().remove(i);
			});
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();

		kingdomManager.getInvitesReceived(player).forEach(i -> {
			i.getSender().sendMessage(KingdomInviteExpireType.TARGET_DISCONNECT.getMessage(player.getName()));
			kingdomManager.getInvites().remove(i);
		});

		kingdomManager.getInvitesSent(player).forEach(i -> {
			i.getTarget().sendMessage(KingdomInviteExpireType.SENDER_DISCONNECT.getMessage(player.getName()));
			kingdomManager.getInvites().remove(i);
		});
	}

}
