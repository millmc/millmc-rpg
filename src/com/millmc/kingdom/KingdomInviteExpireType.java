package com.millmc.kingdom;

public enum KingdomInviteExpireType {

	SENDER_DISCONNECT("§a%0% §7desconectou e o convite foi cancelado!"),
	TARGET_DISCONNECT("§a%0% §7desconectou e o convite foi cancelado!"),
	TIME_OUT_TARGET("§7Convite para entrar no reino de §a%0% §7expirou!"),
	TIME_OUT_SENDER("§7Convite para §a%0% §7entrar no reino expirou!");

	private String message;

	private KingdomInviteExpireType(String message) {
		this.message = message;
	}

	public String getMessage(Object... obj) {
		String msg = message;
		for (int i = 0; i < obj.length; i++) {
			msg = msg.replace("%" + i + "%", obj[i].toString());
		}
		return msg;
	}
}
