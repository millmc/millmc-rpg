package com.millmc.kingdom;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.millmc.Main;
import com.millmc.kingdom.commands.CommandKingdom;
import com.millmc.kingdom.storage.KingdomSQLStorage;
import com.millmc.kingdom.storage.KingdomStorage;
import com.millmcapi.MillAPI;
import com.millmcapi.general.users.User;
import com.millmcapi.general.users.UserManager;

public class KingdomManager {

	private KingdomStorage storage;

	private Set<String> kingdomsName;
	private Map<String, Kingdom> kingdoms;
	private Map<Integer, String> userTag;

	private Set<KingdomInvite> invites;

	private UserManager userManager;

	public KingdomManager() {
		this.storage = new KingdomSQLStorage();
		Set<Kingdom> download = storage.download();
		this.invites = new HashSet<KingdomInvite>();

		this.kingdomsName = new HashSet<String>();
		this.kingdoms = new HashMap<String, Kingdom>();
		this.userTag = new HashMap<Integer, String>();

		download.forEach(i -> {
			kingdoms.put(i.getName(), i);
			kingdomsName.add(i.getName().toLowerCase());

			i.getMembers().forEach(id -> {
				userTag.put(id, i.getName());
			});
		});

		MillAPI millAPI = Main.getInstance().getMillAPI();
		this.userManager = millAPI.getModule().getUserManager();

		millAPI.getCommand().registerCommand(new CommandKingdom(this, userManager));

		Bukkit.getPluginManager().registerEvents(new KingdomListener(this), Main.getInstance());
	}

	public Set<String> getKingdomsName() {
		return this.kingdomsName;
	}

	public Collection<Kingdom> getKingdoms() {
		return this.kingdoms.values();
	}

	public void add(Kingdom kingdom) {
		kingdoms.put(kingdom.getName(), kingdom);
		userTag.put(kingdom.getLeader(), kingdom.getName());
		kingdomsName.add(kingdom.getName().toLowerCase());

		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			storage.add(kingdom);
		});
	}

	public void remove(Kingdom kingdom) {
		kingdoms.remove(kingdom.getName());
		userTag.remove(kingdom.getLeader());
		kingdomsName.remove(kingdom.getName().toLowerCase());

		kingdom.getMembers().forEach(id -> userTag.remove(id));

		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			storage.remove(kingdom);
		});
	}

	public void update(Kingdom kingdom) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			storage.update(kingdom);
		});
	}

	public Map<Integer, String> getUserTag() {
		return this.userTag;
	}

	public Kingdom getKingdomByPlayer(Player player) {
		return getKingdomByName(player.getName());
	}

	public Kingdom getKingdomByName(String name) {
		User user = userManager.getUser(name);
		if (user == null) {
			user = userManager.getUserTemporality(name);
		}
		if (user == null) {
			return null;
		}
		return getKingdomById(user.getId());
	}

	public Kingdom getKingdomById(int id) {
		if (!userTag.containsKey(id)) {
			return null;
		}
		String name = userTag.get(id);
		return kingdoms.get(name);
	}

	public void sendMessage(Kingdom kingdom, String msg) {
		getOnlineMembers(kingdom).forEach(i -> {
			i.getPlayer().sendMessage(msg);
		});
	}

	public Set<User> getOnlineMembers(Kingdom kingdom) {
		Set<User> onlines = new HashSet<User>();
		kingdom.getMembers().stream().forEach(member -> {
			User user = userManager.getUser(member);
			if (user != null) {
				onlines.add(user);
			}
		});
		return onlines;
	}

	//
	//
	// class to invite members to Kingdom
	//

	public Set<KingdomInvite> getInvites() {
		return this.invites;
	}

	public Set<KingdomInvite> getInvitesSent(Player player) {
		return invites.stream().filter(i -> i.getSender().equals(player)).collect(Collectors.toSet());
	}

	public Set<KingdomInvite> getInvitesReceived(Player player) {
		return invites.stream().filter(i -> i.getTarget().equals(player)).collect(Collectors.toSet());
	}
}
